<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('adminHomePage')}}">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class=""></i>
      </div>
      <div class="sidebar-brand-text mx-3"> BABYLON Việt Nam </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
      <a class="nav-link" href="{{route('adminHomePage')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Admin</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Dự Án
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('adminProjectList')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Danh Sách Dự Án</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('adminAddProject')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Thêm Dự Án</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Loại Dự Án
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('adminProjectTypeList')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Danh Sách Loại Dự Án</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('adminAddProjectType')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Thêm Loại Dự Án</span></a>
    </li>

    <!-- Heading -->
    <div class="sidebar-heading">
      Danh Mục Dự Án
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('adminGetCategoryList')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Danh Sách Các Hạng Mục</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('adminGetAddCategory')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Thêm Hạng Mục Dự Án</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
      Loại Dịch Vụ
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('getAdminInsertServiceTypePage')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Thêm Loại Dịch Vụ</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('getAdminServiceTypeList')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Danh Sách Loại Dịch Vụ</span></a>
    </li>
    
    
   
    
    
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
      Tham Khảo
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('getAdminAddReference')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Thêm Bài Tham Khảo</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('getAdminReferences')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Danh Sách Bài Tham Khảo</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
      Slide
    </div>

    <li class="nav-item">
      <a class="nav-link" href="{{route('getAdminSlidesImage')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Hình Ảnh Trong Slide</span></a>
    </li>

    

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>