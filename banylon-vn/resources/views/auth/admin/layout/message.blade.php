
@if(session()->has('success'))
    <div id="message">
        <div class="card mb-4 py-3 border-left-success">
            <div class="card-body">
                    {{ session()->get('success') }}
            </div>
        </div>
    </div>
@endif


@if(session()->has('error'))
    <div id="message">
        <div class="card mb-4 py-3 border-left-warning">
            <div class="card-body">
                {{ session()->get('error') }}
            </div>
        </div>
    </div>
@endif

<style>
 .hide{
     display: none;
 }
</style>
<div id="messageError" class="hide">
    <div class="card mb-4 py-3 border-left-warning">
        <div class="card-body">
            <div id="messageErrorData"></div>
        </div>
    </div>
</div>

<div id="messageSuccess" class="hide">
    <div class="card mb-4 py-3 border-left-success">
        <div class="card-body">
            <div id="messageSuccessData"></div>
        </div>
    </div>
</div>