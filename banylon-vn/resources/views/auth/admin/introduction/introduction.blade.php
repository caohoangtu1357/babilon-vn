@extends('admin.layout.index')

@section('content')

<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Chỉnh Sửa Bài Giới Thiệu</h1>
    </div>
    <form onsubmit="return validateProjectFormBeForSubmit()" name="projectForm" class="user" action="{{route('adminPostAddProject')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">BÀi Giới Thiệu:</h6>
            </div>
            <textarea name="content" class="textarea" id="demo" cols="30" rows="10"></textarea>
        </div>
        
       
       
        
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hình Ảnh Dự Án:</h6>
            </div>
            <input name="imageName[]" type="text" class="form-control imageName" id="exampleInputEmail" placeholder="Tên Hình Ảnh...">
            <input name="imageDescription[]" type="text" class="form-control imageDescription" id="exampleInputEmail" placeholder="Mô Tả Hình Ảnh...">
            <select class="form-control imageType" name="imageType[]">
                <option selected value="" disabled>Chọn Loại Ảnh:</option>
                <option value="1">Hình Đại Diện</option>
                <option value="2">Hình Ảnh Dự Án</option>
                <option value="3">Ảnh Thiết Kế</option>
                <option value="4">Ảnh Sau Thi Công</option>
            </select>
            <input name="imageFile[]" class="form-control imageFile" type="file">
            <button class="btn btn-primary" onclick="duplicate(this)" type="button">Thêm 1 Ảnh Nữa</button>
        </div>
        <div class="col text-center">
            <button type="submit" onclick="submitAddProjectForm()" class="btn btn-primary col-lg-4">
                    Thêm
            </button>
        </div>
        <hr>
    </form>
    <hr>
</div>
@endsection

@section('script')
<script>
    $('.textarea').wysihtml5();
</script>    
@endsection