@extends('admin.layout.index')


@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Thêm Loại Dự Án</h1>
    </div>
    <form class="user" name="formProjectType">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Tên Loại Dự Án:</h6>
            </div>
            <input type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án...">
        </div>

        <div class="col text-center">
            <button type="button" onclick="submitAddProject()" class="btn btn-primary col-lg-4">
                    Thêm Loại Dự Án
            </button>
        </div>
    </form>
    <hr>
  </div>
@endsection

@section('script')
<script>
    function submitAddProject(){
        var formProjectType=document.forms.formProjectType;
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if(validateForm()){
            $.post("them-loai-du-an",{
                name:formProjectType.name.value,
                _token:CSRF_TOKEN
            },
            function(data,status){
                if(status=="success"){
                    formProjectType.name.value="";
                    showSuccessMessage("Đã Thêm Thành Công");
                    closeMessage(5000,"messageSuccessData");
                }
            });
        }else{
            alert("Vui Lòng Điền Tên Loại Dự Án");
        }
            
    }

    function showSuccessMessage(message){
    document.getElementById("messageSuccessData").innerHTML=message;
    document.getElementById("messageSuccess").style.display="block";
    }

    function showErrorMessage(message){
        document.getElementById("messageErorData").innerHTML=message;
        document.getElementById("messageError").style.display="block";
    }

    function closeMessage(time,id){
        setTimeout(function(){
        document.getElementById(id).style.display="none";
        },time)
    }

    function validateForm(){
        var name=document.forms.formProjectType.name.value;
        if(name==""){
            return false;
        }else{
            return true;
        }
    }

</script>

@endsection

