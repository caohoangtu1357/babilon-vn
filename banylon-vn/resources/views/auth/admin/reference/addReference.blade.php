@extends('admin.layout.index')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Thêm Bài Tham Khảo</h1>
    </div>
    <form onsubmit="return validatereferenceFormBeForSubmit()" name="referenceForm" class="user" action="{{route('postAdminAddReference')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Tiêu Đề:</h6>
            </div>
            <input type="text" name="title" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án...">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Nội Dung:</h6>
            </div>
            <textarea class="form-control" name="content" id="" cols="30" rows="8">Nội Dung...</textarea>
        </div>
        
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hình Ảnh:</h6>
            </div>
            <input name="imageDescription" type="text" class="form-control imageDescription" id="exampleInputEmail" placeholder="Mô Tả Hình Ảnh...">
            <input name="imageFile" class="form-control imageFile" type="file">
        </div>
        <div class="col text-center">
            <button type="submit" onclick="submitAddreferenceForm()" class="btn btn-primary col-lg-4">
                    Thêm
            </button>
        </div>
        
        <hr>
    </form>
    <hr>
</div>
@endsection

@section('script')
<script>
    function duplicate(id) {
    var i = 0;
    var original = id.parentNode;
    var clone = original.cloneNode(true); 
    original.appendChild(clone);
    }
</script>

<script>
    function validatereferenceFormBeForSubmit(){
        var referenceForm=document.forms.referenceForm;
        if(referenceForm.title.value==""){
            alert("Vui Lòng Kiểm Tra Tiêu Đề");
            return false;
        }
        if(referenceForm.content.value==""){
            alert("Vui Lòng Kiểm Tra Nội Dung");
            return false;
        }
  
        if(referenceForm.imageDescription.value==""){
            alert("Vui Lòng Kiểm Tra Mô Tả Hình Ảnh");
            return false;
        }
        if(referenceForm.imageFile.value==""){
            alert("Vui Lòng Kiểm Tra Tệp Hình Ảnh");
            return false;
        }
        return true;

    }

    
</script>
@endsection

