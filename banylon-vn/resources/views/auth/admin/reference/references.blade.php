@extends('admin.layout.index')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Danh Sách Bài Tham Khảo</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Danh Sách Bài Tham Khảo</h6>
      </div>
      <div class="card-body">
        <!-- will print here-->
        <div class="table-responsive" id="printHere">
            <div class="input-group">
                <input type="text" data-table="order-table" class="form-control bg-light border-0 small light-table-filter" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
          <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
              <tr align="center">
                <th>STT</th>
                <th>Tiêu Đề</th>
                <th>Mô Tả</th>
                <th>Hình Ảnh</th>
                <th>Chỉnh Sửa</th>
              </tr>
            </thead>
            <tbody align="center">
                @foreach ($references as $key=>$reference)
                  <tr>
                    <td style="vertical-align:middle">{{$key+1}}</td>
                    <td style="vertical-align:middle">{{$reference->title}}</td>
                    <td style="vertical-align:middle">{{$reference->imageDescription}}</td>
                    <td style="vertical-align:middle"><img src="upload/imageReference/{{$reference->imageLink}}" alt="" width="80px" height="auto"></td>
                    
                    <td style="vertical-align:middle">
                      <div class="form-group">
                        <a href="{{route('getAdminReferenceDetail',$reference->id)}}" class="btn btn-info btn-icon-split col-lg-8">
                          <span class="text">Chỉnh Sửa</span>
                        </a>
                        <button onclick="deleteReference({{$reference->id}},this)" class="btn btn-danger btn-icon-split col-lg-8">
                          <span class="text">Xóa</span>
                        </button>
                      </div>
                    </td>
                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>

@endsection


@section('script')
  <script>
      (function(document) {
      'use strict';
      var LightTableFilter = (function(Arr) {
        var _input;
        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }
        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }
        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);
      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });
    })(document);
  </script>

  <script>
    function deleteReference(idReference,element){
      $.get("xoa-bai-tham-khao/"+idReference,function(data,status){
        if(status=="success"){
          showSuccessMessage("Xóa Thành Công");
          element.parentNode.parentNode.parentNode.remove();
          closeMessage(5000); 
        }
      });
    }

    function showSuccessMessage(message){
      document.getElementById("messageSuccessData").innerHTML=message;
      document.getElementById("messageSuccess").style.display="block";
    }

    function closeMessage(time){
      setTimeout(function(){
        document.getElementById("messageSuccess").style.display="none";
      },time)
    }
  </script>
@endsection