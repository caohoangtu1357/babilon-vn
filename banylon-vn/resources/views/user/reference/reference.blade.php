@extends('user.layout.index')

@section('content')
    <!--header-->
    <div class="header">
        <!-- carousel-->
        <div class="carousel" id="mySlides">
            <div class="carousel-caption"><h1>Tham Khảo</h1></div>
            @foreach ($slides as $slide)
                <div class="slide" >
                    <div class="slide-img">
                        <img class="fade"src="{{URL::asset("upload/imageSlide")}}/{{$slide->imageLink}}" alt="{{$slide->description}}" />
                    </div>
                </div>
            @endforeach
            
            <div class="currentSlide">
                <span class="dot slide-active" onclick="currentSlide(1)"></span> 
                @foreach ($slides as $key=>$slide)
                    @if ($key!=0)
                        <span class="dot" onclick="currentSlide({{$key+1}})"></span> 
                    @endif
                @endforeach
            </div> 
            <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
            <a class="next" onclick="plusSlide(1)">&#10095;</a>
        </div>
        <!-- end carousel-->
        <!-- navigation-->
        @include('user.layout.header')	
    
    <!-- end menu mobile-->
    </div>
    <!--end header-->

    <!----------start content reference-------------->
    <div class="container-reference">
        <h1>Bài viết nổi bật</h1>
        <div class="wrap-refer-content">
            <div class="refer-content-left">
                @foreach ($references as $key=>$reference)
                    @if ($key%2==0)
                        <div class="refer-content">
                            <div class="refer-img">
                                <img src="{{URL::asset('upload/imageReference')}}/{{$reference->imageLink}}" alt="{{$reference->imageDescription}}" />
                            </div>
                            <h2 class="refer-title">{{$reference->title}}</h2>
                            <div class="content">
                                <p id="content{{$key+1}}" class="content">{{$reference->content}}</p>
                            <div class="refer-extend"><a id="btn-extend{{$key+1}}" href="javascript:void(0)" onclick="extendContent(1)">Xem thêm</a></div>
        
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="refer-content-right">
                @foreach ($references as $key=>$reference)
                    @if($key%2!=0) 
                        <div class="refer-content">  
                            <div class="refer-img">
                                <img src="{{URL::asset('upload/imageReference')}}/{{$reference->imageLink}}" alt="{{$reference->imageDescription}}" />
                            </div>
                            <h2 class="refer-title">{{$reference->title}}</h2>
                            <div class="content">
                                <p id="content{{$key+1}}" class="content">{{$reference->content}}</p>
                                <div class="refer-extend"><a id="btn-extend{{$key+1}}" href="javascript:void(0)" onclick="extendContent(3)">Xem thêm</a></div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
                    
                
            
            
            
            <div class="clear"></div>
        </div>
    </div>
    <!-----------end content reference----------->
@endsection