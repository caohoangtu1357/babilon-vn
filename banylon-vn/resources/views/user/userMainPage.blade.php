@extends('user.layout.index')
@section('content')

    <div class="header">
        <!-- carousel-->
        <div class="carousel" id="mySlides">
            <div class="carousel-caption"><h1>KIẾN TRÚC - XÂY DỰNG - NỘI THẤT</h1></div>
            @foreach ($slides as $slide)
                <div class="slide" >
                    <div class="slide-img">
                        <img class="fade"src="{{URL::asset("upload/imageSlide")}}/{{$slide->imageLink}}" alt="{{$slide->description}}" />
                    </div>
                </div>
            @endforeach
            
            <div class="currentSlide">
                <span class="dot slide-active" onclick="currentSlide(1)"></span> 
                @foreach ($slides as $key=>$slide)
                    @if ($key!=0)
                        <span class="dot" onclick="currentSlide({{$key+1}})"></span> 
                    @endif
                @endforeach
            </div> 
            <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
            <a class="next" onclick="plusSlide(1)">&#10095;</a>
        </div>
        <!-- end carousel-->
        <!-- navigation-->
        @include('user.layout.header')
        <!--end navigation-->
    </div>


    <!---main-content--->
    <div class="container">
        <!-------- project-------->
        <div class="container-project">
            <div class="container-project-content">
                <h3>Tầm nhìn và sứ mệnh</h3>
                <p><strong>BABYLON</strong> đem lại phương án tối ưu cho không gian sống của bạn, phù hợp với nhu cầu của từng cá nhân trong gia đình . Hãy bắt đầu trải nghiệm dịch vụ của chúng tôi để xây dựng không gian mơ ước của bạn.</p>
            </div>
            <div class="container-project-image">
                <div class="row">
                    @foreach ($thumbnailEachProject as $thumbnail)
                    <div class="col-3 col-md-4 col-sm-6">
                        <p class="project-name">{{$thumbnail->projectName}}</p>
                            <div><a href="{{route('userGetProjectDetail',$thumbnail->slug)}}"><img src="{{URL::asset("upload/imageProject")}}/{{$thumbnail->imageLink}}" alt="{{$thumbnail->description}}" /></a></div>
                        </div>
                    @endforeach
                </div>
                <p class="all-project"><a href="{{route('userGetAllProject')}}">Xem tất cả</a></p>
            </div>
        </div>
        <!------------- end project--------------->
        <!---------------price------------------>
        <div class="container-price" id="price">
            <div class="container-price-content">
                <h3>Hỗ trợ chu đáo, chi phí hợp lý</h3>
                <p><strong>BABYLON</strong> với thế mạnh Design & Buid, chúng tôi hiểu biết sâu sắc trong lĩnh vực của mình, sẽ mang đến cho bạn công trình mơ ước với chi phí hợp lý nhất, giúp bạn tiết kiệm đáng kể công sức và thời gian.
                </p>
            </div>
            <div class="row">
                @foreach ($serviceTypes as $serviceType)
                    <div class="col-3 col-s-6">
                        <div class="price-title bg-gray">
                            <div class="price-title-top">
                                <h2>{{$serviceType->name}}</h2>
                            </div>
                            <div class="price-title-bot">
                                @foreach ($serviceType->price as $priceItem)
                                    <p class="title">{{ number_format($priceItem->cost)}} Vnđ/m2</p>
                                    <p>{{$priceItem->name}}</p>
                                @endforeach
                            </div>
                        </div>
                        <div class="price-content">
                            @foreach ($serviceType->service as $serviceItem)
                                <p><span>&#10003;</span>{{$serviceItem->name}}</p> 
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="col-3 col-s-6">
                    <div class="price-title bg-orange">
                        <div class="price-title-top bd-bottom-black">
                            <h2 class="color-red">Giảm chi phí thiết kế</h2>
                        </div>
                        <div class="price-title-bot color-red">
                            
                            <p>Khi ký hợp đồng thi công</p>
                            <p>Đảm bảo sản phẩm thực tế đạt trên 80% thiết kế.</p>
                            
                        </div>
                    </div>
                    <div class="price-content">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------end main-content------------>
@endsection
