@extends('user.layout.index')
@section('content')
    <!--header-->
    <div class="header">
            <!-- carousel-->
            <div class="carousel" id="mySlides">
                <div class="carousel-caption"><h1>Giới Thiệu</h1></div>
                @foreach ($slides as $slide)
                    <div class="slide" >
                        <div class="slide-img">
                            <img class="fade"src="{{URL::asset("upload/imageSlide")}}/{{$slide->imageLink}}" alt="{{$slide->description}}" />
                        </div>
                    </div>
                @endforeach
                
                <div class="currentSlide">
                    <span class="dot slide-active" onclick="currentSlide(1)"></span> 
                    @foreach ($slides as $key=>$slide)
                        @if ($key!=0)
                            <span class="dot" onclick="currentSlide({{$key+1}})"></span> 
                        @endif
                    @endforeach
                </div> 
                <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
                <a class="next" onclick="plusSlide(1)">&#10095;</a>
            </div>
            @include('user.layout.header')	
        
        <!-- end menu mobile-->
        </div>
        <!--end header-->


        <!-------------content introduce------------->
        <div class="intro-container">
            <div class="intro-content">
                <p>
                    BABYLON là công ty chuyên hoạt động về lĩnh vực thiết kế, thi công công trình kiến trúc xây dựng; thiết kế, sản xuất và thi công nội thất. Với nhân sự
                    có chuyên môn cao chúng tôi tin rằng một thiết kế xuất sắc kết hợp với quá trình thi công bài bản sẽ tạo nên một công trình tốt
                </p>
                <p>
                    Một dự án được thực hiện thành công từ khâu thiết kế đến khi hoàn thành đòi hỏi phải có sự phối hợp đồng bộ giữa chủ đầu tư và nhà thầu
                </p>
                <p>
                    Hiểu được điều đó, chúng tôi cung cấp tới khách hàng dịch vụ tư vấn một cách toàn diện bao gồm các giải pháp cảnh quan tổng thể, kiến trúc, kết
                    cấu, m&e, nội thất v.v...trong khả năng tài chính của khách hàng. Chúng tôi làm việc với khách hàng để xác định chính xác những yêu cầu và mong
                    muốn của khách hàng và đảm bảo khách hàng đang nhận được những sản phẩm tư vấn có chất lượng tốt nhất trong khả năng tài chính của mỗi
                    khách hàng.

                </p>
                <p>
                    Kinh nghiệm và đam mê của chúng tôi sẽ đem đến cho bạn sự hài lòng với không gian sống và chi phí đầu tư tốt nhất.
                </p>
            </div>
            <div class="intro-employees">
                <h2>NHÂN SỰ CHỦ CHỐT</h2>
                <div class="row">
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/trung.jpg" /></div>
                        <div class="employees-info">
                            <ul >
                                <li>ĐẶNG HÀ TRUNG</li>
                                <li>Giám đốc</li>
                                <li>Thạc sỹ, kiến trúc sư</li>
                                <li>Đại học kiến trúc</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/tien.jpg" /></div>
                        <div class="employees-info">
                            <ul>
                                <li>VŨ NGỌC TIẾN</li>
                                <li>Phó giám đốc</li>
                                <li>Kỹ sư xây dựng</li>
                                <li>Đại học bách khoa</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/phi.jpg" /></div>
                        <div class="employees-info">
                            <ul>
                                <li>NGUYỄN NGỌC PHI</li>
                                <li>Trưởng phòng kỹ thuật</li>
                                <li>Kỹ sư xây dựng</li>
                                <li>Đại học bách khoa</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/tri.jpg" /></div>
                        <div class="employees-info">
                            <ul>
                                <li>BÙI MINH TRÍ</li>
                                <li>Trưởng phòng thiết kế</li>
                                <li>kiến trúc sư</li>
                                <li>Đại học kiến trúc</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/thanh.jpg" /></div>
                        <div class="employees-info">
                            <ul >
                                <li>NGUYỄN TẤN THANH</li>
                                <li>Thiết kế</li>
                                <li>Họa viên</li>
                                <li>Đại học FPT</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-2 col-md-4 col-sm-6">
                        <div class="employees-img"><img src="{{URL::asset('upload/imageEmployee')}}/tan.jpg" /></div>
                        <div class="employees-info">
                            <ul>
                                <li>PHẠM VĂN TÂN</li>
                                <li>Đội trưởng</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!------------end content introduce------------->
@endsection