<!DOCTYPE html> 
<html> 
    <head>
      <meta charset="utf-8">
      <meta content="Content-Type: text/html">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Công Ty Cổ Phần Đầu Tư Xây Dựng BABYLON</title>
      <meta name="description" content="Thiết kế thi công xây dựng nhà phố, biệt thự trọn gói. Thiết kế thi công nội thất căn hộ chung cư trọn gọi.">
      <!--<meta name="keywords" content="Kiến trúc, xây dựng, nội thất"> -->
      <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
      <link rel="stylesheet " type="text/css"  href="{{URL::asset("assets_user")}}/css/style.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset("assets_user")}}/css/responsive.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset("assets_user")}}/css/reset.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset("assets_user")}}/css/du-an.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <LINK REL="SHORTCUT ICON"  HREF="{{URL::asset("assets_user/logo/babylonlogo.png")}}">
      <meta property="og:title" content="Công Ty Cổ Phần Đầu Tư Xây Dựng BABYLON">
      <meta property="og:description" content="Thiết kế thi công xây dựng nhà phố, biệt thự trọn gói. Thiết kế thi công nội thất căn hộ chung cư trọn gọi.">
      <meta property="og:type" content="website">
      <meta property="og:site_name" content="Babylon - Kiến trúc, xây dựng và nội thất">
      <meta property="og:locale" content="vi_VN">
      <meta property="og:image" content="http://babylonvietnam.vn/assets_user/logo/logoShare.png ">
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="300" />
      <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
<body> 
    <div class="wrap" id="wrapper">
        <!--header--> 

        @yield('header')
        <!--end header--> 
        
        @yield('content')
        <!--footer--> 
        @include('user.layout.footer')
        <!--end footer-->
    </div>


@yield('script')

<script src="{{URL::asset("assets_user")}}/javascript/text.js" ></script>

</body> 
</html>	 
