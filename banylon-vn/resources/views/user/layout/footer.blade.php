<div class="footer" id="lien-he">
        <hr>
        <br>
        <div class="footer-left">
            <h2>CAM KẾT</h2>
            <ul>
                <li>Với lợi thế về Design & Build chúng tôi sẽ mang đến một sản phẩm phù hợp nhất với nhu cầu của bạn.</li>
                <li>Sẵn sàng hỗ trợ mọi lúc và đưa ra lời khuyên phù hợp nhất.</li>
                <li>Chúng tôi sẽ mang đến cho bạn sản phẩm tốt nhất với chi phi hợp lý.</li>
                <li> Cung cấp đầy đủ các dịch vụ cho đến khi bạn dọn nhà vào ở.</li>
            </ul>
        </div>
        <div class="footer-right">
            <h2>TRỤ SỞ CHÍNH</h2>
            <ul>
                <li> 27/2A3 Huỳnh Tịnh Của, P8, Q3, Tp.HCM</li>
                <li> Email : support@babylonvietnam.vn</li>
                <li> Hotline : 0932 131 694</li>
                <div class="social"><span ><a href="https://www.facebook.com/babylonvietnam.vn/" target="_blank"><i class="fa fa-facebook-official"></i></a></span></div>
            </ul>
        </div>
        
        <div class="clear"></div>
        <br>
        <hr>
        <div class="footer-logo">
            <img src="{{URL::asset("assets_user")}}/logo/babylonlogo.png" />
        </div>
        
    
    </div>
    <div class="hotline"><i class="fa fa-phone"></i> Hotline : <a>0932 131 694</a> (Hỗ trợ miễn phí)</div> 
