
	<!-- navigation-->
	<div class="nav">
			<div class="nav-left">
				<a href="{{route('userMainPage')}}"><img id="change-logo" class="logo" src="http://babylonvietnam.vn/assets_user/logo/Babylonlogo1.png" /></a>
			</div>
			<div class="nav-right">
				<!-- menu pc---->

				<ul class="menu">
					<li><a href="#">Hotline: 0932 131 694</a></li>
					<li><a href="{{route('userGetIntroductionPage')}}">Giới thiệu</a></li>
					<li>
						<a href="{{route('userGetAllProject')}}">Dự án</a>
						<ul class="sub-menu" id="mymenu">
							@foreach ($projectTypeList as $projectType)
								<li><a href="{{route('userProjectsByProjectTypeId',$projectType->slug)}}">{{$projectType->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{route('userMainPage')}}#price">Bảng giá</a></li>						
					<li><a href="{{route('userGetReferencePage')}}">Tham khảo</a></li>
					<li><a href="{{route('userMainPage')}}#lien-he">Liên hệ</a></li>
					
				</ul>
				<div class="icon-menu-mobile" onclick="openAndCloseMenu()"><i id="icon" class="fa fa-bars"></i></div>

				<!-- end- menupc-->
			</div>
			<div class="clear"></div>
		</div>
		<!--end navigation-->
		<!-- menu mobile-->
		<div class="menu-mobile" id="menu-mb">
				<ul>
					<li><a href="#">Hotline: 0932 131 694</a></li>
					<li><a href="{{route('userGetIntroductionPage')}}">Giới thiệu</a></li>
					<li><a href="{{route('userMainPage')}}#price">Bảng giá</a></li>
					<li>
						<a href="#">Dự án</a>
						<ul class="sub-menumobile" id="mymenu-mobile">
							@foreach ($projectTypeList as $projectType)
								<li><a href="{{route('userProjectsByProjectTypeId',$projectType->slug)}}">{{$projectType->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{route('userGetReferencePage')}}">Tham khảo</a></li>
					<li><a href="{{route('userMainPage')}}#lien-he">Liên hệ</a></li>
				</ul>
		</div>		
	
	<!-- end menu mobile-->

	