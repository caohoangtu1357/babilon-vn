@extends('user.layout.index')

@section('content')
	<div class="header">
		<!-- carousel-->
		<div class="carousel" id="mySlides">
			<div class="carousel-caption"><h1></h1></div>
			@foreach ($slides as $slide)
				<div class="slide" >
					<div class="slide-img">
						<img class="fade"src="{{URL::asset("upload/imageSlide")}}/{{$slide->imageLink}}" alt="{{$slide->description}}" />
					</div>
				</div>
			@endforeach
			
			<div class="currentSlide">
				<span class="dot slide-active" onclick="currentSlide(1)"></span> 
				@foreach ($slides as $key=>$slide)
					@if ($key!=0)
						<span class="dot" onclick="currentSlide({{$key+1}})"></span> 
					@endif
				@endforeach
			</div> 
			<a class="prev" onclick="plusSlide(-1)">&#10094;</a>
			<a class="next" onclick="plusSlide(1)">&#10095;</a>
		</div>
		<!-- end carousel-->
		<!-- navigation-->
		@include('user.layout.header')
		<!--end navigation-->
	</div>

	<div class="container-project" id="projectTypesTag">
		<div class="project-tag">
			<ul class="list-project-tag">
				@foreach ($projectTypes as $projectType)
					<li><a href="{{route('userProjectsByProjectTypeId',$projectType->slug)}}/#projectTypesTag">{{$projectType->name}}</a></li>
				@endforeach
			</ul>
		</div>
		
		<div class="container-project-image">
			<div class="row">
				@foreach ($projects as $project)
					<div class="col-3 col-md-4 col-sm-6">
						<p class="project-name">{{$project->projectName}}</p>
						<div><a href="{{route('userGetProjectDetail',$project->slug)}}"><img src="{{ URL::asset('upload/imageProject/')}}/{{$project->imageLink}}" alt="{{$project->imageDescription}}"></a></div>
					</div>
				@endforeach
				
			</div>
			
		</div>
	</div>
@endsection

@section('script')
    <script>
        function displayTitle(element){
            console.log( element.childNodes);
        }
    
    
          
    /*------------change logo ----------------*/

    var x = window.matchMedia("(max-width: 810px)")
    function myFunction(x) {
      if (x.matches) 
      {
       // If media query matches
     
            setTimeout(function(){
                var logo= document.getElementById('change-logo');
                logo.src="http://babylonvietnam.vn/assets_user/logo/babylonlogo.png";
            },1000);
            
       
        //document.getElementById('change-logo').src="assets_user/logo/babylonlogo.png";
      }
      else
        setTimeout(function(){
                var logo= document.getElementById('change-logo');
                logo.src="http://babylonvietnam.vn/assets_user/logo/Babylonlogo1.png";
            },1000);
        //document.getElementById('change-logo').src="assets_user/logo/Babylonlogo1.png";
    }
    
    
    myFunction(x) // Call listener function at run time
    x.addListener(myFunction) // Attach listener function on state changes
    /* ----------------------end changelogo------------*/
    
        
    
        
    </script>

@endsection
