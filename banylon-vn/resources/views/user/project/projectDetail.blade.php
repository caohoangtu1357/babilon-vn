@extends('user.layout.index')


@section('content')
    <!--header-->
    <div class="header">
        <!-- carousel-->
        <div class="carousel" id="mySlides">
            <div class="carousel-caption"><h1>{{$projectDetail->name}}</h1></div>
            @foreach ($allImage as $image)
                <div class="slide" >
                    <div class="slide-img">
                        <img class="fade" src="{{ URL::asset('upload/imageProject/')}}/{{$image->imageLink}}" alt="{{$image->description}}" />
                    </div>
                </div>
            @endforeach
            
            <div class="currentSlide">
                <span class="dot slide-active" onclick="currentSlide(1)"></span>
                @foreach ($allImage as $key=>$item)
                    @if ($key!=0)
                        <span class="dot" onclick="currentSlide({{$key+1}})"></span> 
                    @endif
                @endforeach 
            </div> 
            <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
            <a class="next" onclick="plusSlide(1)">&#10095;</a>
        </div>
        <!-- end carousel-->
        <!-- navigation-->
      
	<!-- navigation-->
	<div class="nav">
			<div class="nav-left">
				<a href="{{route('userMainPage')}}"><img id="change-logo" class="logo" src="http://babylonvietnam.vn/assets_user/logo/Babylonlogo1.png" /></a>
			</div>
			<div class="nav-right">
				<!-- menu pc---->

				<ul class="menu">
					<li><a href="#">Hotline: 0932 131 694</a></li>
					<li><a href="{{route('userGetIntroductionPage')}}">Giới thiệu</a></li>
					<li>
						<a href="{{route('userGetAllProject')}}">Dự án</a>
						<ul class="sub-menu" id="mymenu">
							@foreach ($projectTypeList as $projectType)
								<li><a href="{{route('userProjectsByProjectTypeId',$projectType->id)}}">{{$projectType->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{route('userMainPage')}}#price">Bảng giá</a></li>						
					<li><a href="{{route('userGetReferencePage')}}">Tham khảo</a></li>
					<li><a href="{{route('userMainPage')}}#lien-he"">Liên hệ</a></li>
					
				</ul>
				<div class="icon-menu-mobile" onclick="openAndCloseMenu()"><i id="icon" class="fa fa-bars"></i></div>

				<!-- end- menupc-->
			</div>
			<div class="clear"></div>
		</div>
		<!--end navigation-->
		<!-- menu mobile-->
		<div class="menu-mobile" id="menu-mb">
				<ul>
					<li><a href="#">Hotline: 0932 131 694</a></li>
					<li><a href="{{route('userGetIntroductionPage')}}">Giới thiệu</a></li>
					<li><a href="{{route('userMainPage')}}#price">Bảng giá</a></li>
					<li>
						<a href="#">Dự án</a>
						<ul class="sub-menumobile" id="mymenu-mobile">
							@foreach ($projectTypeList as $projectType)
								<li><a href="{{route('userProjectsByProjectTypeId',$projectType->id)}}">{{$projectType->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{route('userGetReferencePage')}}">Tham khảo</a></li>
					<li><a href="{{route('userMainPage')}}#lien-he"">Liên hệ</a></li>
				</ul>
		</div>		
	
	<!-- end menu mobile-->

	
        <!--end navigation-->
        <!-- menu mobile-->
        <div class="menu-mobile" id="menu-mb">
                <ul>
                    <li><a href="#">Hotline: 0932 131 694</a></li>
                    <li><a href="#">Giới thiệu</a></li>
                    <li><a href="#">Bảng giá</a></li>
                    <li>
                        <a href="#">Dự án</a>
                        <ul class="sub-menumobile" id="mymenu-mobile">
                            <li><a href="#">Ảnh thực tế sau thi công</a></li>
                            <li><a href="#">Biệt thự, nhà phố</a></li>
                            <li><a href="#">building</a></li>
                            <li><a href="#">Căn hộ, penthouse</a></li>
                            
                        </ul>
                    </li>
                    <li><a href="#">Tham khảo</a></li>
                    <li><a href="#">Liên hệ</a></li>
                </ul>
        </div>		
    
    <!-- end menu mobile-->
    </div>
    <!--end header-->

    <!-------project detail---------->
    <div class="wrap-project-detail" id="project-detail">
        <div class="project-info">
            <h2>Thông tin dự án</h2>
            <ul>
                <li>Chủ đầu tư: {{$projectDetail->customerName}}</li>
                <li>Địa chỉ: {{$projectDetail->address}}</li>
                <li>Hạng mục: {{$projectDetail->categoryName}}</li>
                <li>Năm thực hiện: {{$projectDetail->startYear}}</li>
            </ul>
        </div>
        <div class="project-img-container">
            

            @isset($projectThumbnail)
            <div class="wrap-img">
                <img src="{{ URL::asset('upload/imageProject/')}}/{{$projectThumbnail->imageLink}}" alt="{{$projectThumbnail->description}}">
                <p class="description-img">{{$projectThumbnail->description}}</p>
            </div>
            @endisset

            @foreach ($projectImages as $projectImage)
                <div class="wrap-img">
                    <img src="{{ URL::asset('upload/imageProject/')}}/{{$projectImage->imageLink}}" alt="{{$projectImage->description}}">
                    <p class="description-img">{{$projectImage->description}}</p>
                </div>
            @endforeach

            @foreach ($projectAfterConstructImage as $projectImage)
                <div class="wrap-img">
                    <img src="{{ URL::asset('upload/imageProject/')}}/{{$projectImage->imageLink}}" alt="{{$projectImage->description}}">
                    <p class="description-img">{{$projectImage->description}}</p>
                </div>
            @endforeach

            @foreach($projectDesignImage as $designImage)
                <div class="wrap-img">
                    <img src="{{ URL::asset('upload/imageProject/')}}/{{$designImage->imageLink}}" alt="{{$designImage->description}}">
                    <p class="description-img">{{$designImage->description}}</p>
                </div>
            @endforeach
        </div>
    </div>
    
    <!-------end project detail-------->
@endsection



@section('script')
    <script>
    /*------------change logo ----------------*/

    var x = window.matchMedia("(max-width: 810px)")
    function myFunction(x) {
      if (x.matches) 
      {
       // If media query matches
     
            setTimeout(function(){
                var logo= document.getElementById('change-logo');
                logo.src="http://babylonvietnam.vn/assets_user/logo/babylonlogo.png";
            },1000);
            
       
        //document.getElementById('change-logo').src="assets_user/logo/babylonlogo.png";
      }
      else
        setTimeout(function(){
                var logo= document.getElementById('change-logo');
                logo.src="http://babylonvietnam.vn/assets_user/logo/Babylonlogo1.png";
            },1000);
        //document.getElementById('change-logo').src="assets_user/logo/Babylonlogo1.png";
    }
    
    
    myFunction(x) // Call listener function at run time
    x.addListener(myFunction) // Attach listener function on state changes
    /* ----------------------end changelogo------------*/
    /*
        (function(){
            setTimeout(function(){
                var logo= document.getElementById('change-logo');
                logo.src="http://babylonvietnam.vn/assets_user/logo/Babylonlogo1.png?fbclid=IwAR3XgtclLst-thlyssQWtbw9IDPgP8mdvKjuQKRkwKILqOwCWs9eVEr-JBM";
            },1000);
            
        })();
        */
        
    </script>
@endsection





