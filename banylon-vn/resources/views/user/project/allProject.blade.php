@extends('user.layout.index')

@section('content')
	<div class="header">
		<!-- carousel-->
		<div class="carousel" id="mySlides">
			<div class="carousel-caption"><h1></h1></div>
			@foreach ($slides as $slide)
				<div class="slide" >
					<div class="slide-img">
						<img class="fade"src="{{URL::asset("upload/imageSlide")}}/{{$slide->imageLink}}" alt="{{$slide->description}}" />
					</div>
				</div>
			@endforeach
			
			<div class="currentSlide">
				<span class="dot slide-active" onclick="currentSlide(1)"></span> 
				@foreach ($slides as $key=>$slide)
					@if ($key!=0)
						<span class="dot" onclick="currentSlide({{$key+1}})"></span> 
					@endif
				@endforeach
			</div> 
			<a class="prev" onclick="plusSlide(-1)">&#10094;</a>
			<a class="next" onclick="plusSlide(1)">&#10095;</a>
		</div>
		<!-- end carousel-->
		<!-- navigation-->
		@include('user.layout.header')
		<!--end navigation-->
	</div>

	<div class="container-project">
		<div class="project-tag">
			<ul class="list-project-tag">
				@foreach ($projectTypes as $projectType)
					<li><a href="{{route('userProjectsByProjectTypeId',$projectType->slug)}}">{{$projectType->slug}}</a></li>
				@endforeach
			</ul>
		</div>
		
		<div class="container-project-image">
			<div class="row">
				@foreach ($thumbnailEachProject as $thumbnail)
					<div class="col-3 col-md-4 col-sm-6">
						<p class="project-name">{{$thumbnail->projectName}}</p>
						<div><a href="{{route('userGetProjectDetail',$thumbnail->slug)}}/#project-detail"><img src="{{ URL::asset('upload/imageProject/')}}/{{$thumbnail->imageLink}}" alt="{{$thumbnail->description}}"></a></div>
					</div>
				@endforeach
				
			</div>
			
		</div>
	</div>
@endsection

@section('script')
    <script>
        function displayTitle(element){
            console.log( element.childNodes);
        }
    </script>
@endsection
