@extends('admin.layout.index')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Danh Sách Hình Ảnh Trong Slide</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Danh Sách Hình Ảnh Trong Slide</h6>
      </div>
      <div class="card-body">
        <!-- will print here-->
        <div class="table-responsive" id="printHere">
            <div class="input-group">
                <input type="text" data-table="order-table" class="form-control bg-light border-0 small light-table-filter" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                <th>STT</th>
                <th>Mô Tả Hình Ảnh</th>
                <th>Xóa</th>
                </tr>
            </thead>
            <tbody align="center">
                @foreach ($slides as $key=>$slide)
                    <tr>
                    <td style="vertical-align:middle">{{$key+1}}</td>
                    <td style="vertical-align:middle">{{$slide->description}}</td>
                    <td style="vertical-align:middle"><img src="{{ URL::asset('upload/imageSlide/')}}/{{$slide->imageLink}}" width="20%" height="auto"></td>
                    <td style="vertical-align:middle">
                        <div class="form-group">
                        <button onclick="deleteImageSlide({{$slide->id}},this)" class="btn btn-danger btn-icon-split col-lg-8">
                            <span class="text">Xóa</span>
                        </button>
                        </div>
                    </td>
                    </tr>
                @endforeach
            </tbody>
            </table>
        </div>
        </div>
    </div>

    <form onsubmit="return validateForm()" action="{{route('postAdminAddSlideImage')}}" name="addImageForm" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <br>
            <div>
                <h6 class="font-weight-bold text-primary">Ảnh Slide:</h6>
            </div>
            <input name="imageDescription[]" type="text" class="form-control imageDescription" id="exampleInputEmail" placeholder="Mô Tả Hình Ảnh...">
           
            <input name="imageFile[]" class="form-control imageFile" type="file">
            <button class="btn btn-primary" onclick="duplicate(this)" type="button">Thêm 1 Ảnh Nữa</button>
        </div>
        <div align="center">
            <input type="submit" class="form-control btn btn-primary col-lg-6" value="Thêm">
        </div>
    </form>

</div>

@endsection


@section('script')
  <script>
      (function(document) {
      'use strict';
      var LightTableFilter = (function(Arr) {
        var _input;
        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }
        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }
        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);
      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });
    })(document);
  

    function duplicate(id) {
    var i = 0;
    var original = id.parentNode;
    var clone = original.cloneNode(true); 
    original.appendChild(clone);
    }
  

    function deleteImageSlide(idImage,element){
      $.get("xoa-hinh-anh-trong-slide/"+idImage,function(data,status){
        console.log(data,status);
        if(status=="success"){
          showSuccessMessage(data.status);
          element.parentNode.parentNode.parentNode.remove();
          closeMessage(5000); 
        }
      });
    }


    function showSuccessMessage(message){
      document.getElementById("messageSuccessData").innerHTML=message;
      document.getElementById("messageSuccess").style.display="block";
    }


    function closeMessage(time){
      setTimeout(function(){
        document.getElementById("messageSuccess").style.display="none";
      },time)
    }

    function validateForm(){
        var imageFile= document.getElementsByClassName('imageFile');
        var imageDescription=document.getElementsByClassName('imageDescription');
        for(let i=0;i<imageFile.length;i++){
            if(imageFile[i].value!=""){
                if(imageDescription[i].value==""){
                    alert("Vui Lòng Kiểm Tra Mô Tả Hình Ảnh");
                    return false;
                }
            }
        }

        for(let i=0;i<imageDescription.length;i++){
            if(imageDescription[i].value!=""){
                if(imageFile[i].value==""){
                    alert("Vui Lòng Kiểm File Hình Ảnh");
                    return false;
                }
            }
        }
        return true;
    }
  </script>
@endsection