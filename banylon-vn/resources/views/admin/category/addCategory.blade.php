@extends('admin.layout.index')


@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Thêm Hạng Mục Dự Án</h1>
    </div>
    <form class="user" name="formCategory">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Tên Hạn Mục Dự Án:</h6>
            </div>
            <input type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Hạng Mục...">
        </div>

        <div class="col text-center">
            <button type="button" onclick="submitAddProject()" class="btn btn-primary col-lg-4">
                    Thêm Hạng Mục
            </button>
        </div>
    </form>
    <hr>
  </div>
@endsection

@section('script')
<script>
    function submitAddProject(){
        var formCategory=document.forms.formCategory;
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if(validateForm()){
            $.post("them-hang-muc-du-an",{
                name:formCategory.name.value,
                _token:CSRF_TOKEN
            },
            function(data,status){
                if(status=="success"){
                    var messageData=console.log(document.getElementById("messageData"))
                    formCategory.name.value="";
                    showSuccessMessage("Hạng Mục Dự Án Được Thêm Thành Công");
                    closeMessage(5000,"messageSuccess");
                }
            });
        }else{
            alert("Vui Lòng Nhập Hạng Mục Dự Án");
        }
            
    }

    function validateForm(){
        var name=document.forms.formCategory.name.value;
        if(name==""){
            return false;
        }else{
            return true;
        }
    }

    function showSuccessMessage(message){
    document.getElementById("messageSuccessData").innerHTML=message;
    document.getElementById("messageSuccess").style.display="block";
    }

    function showErrorMessage(message){
        document.getElementById("messageErorData").innerHTML=message;
        document.getElementById("messageError").style.display="block";
    }

    function closeMessage(time,id){
        setTimeout(function(){
        document.getElementById(id).style.display="none";
        },time)
    }

</script>

@endsection

