@extends('admin.layout.index')

@section('content')
<style>

    body {font-family: Arial, Helvetica, sans-serif;}
    
    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    
    /* Modal Content */
    .modal-content {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
      border-radius: 20px;
    }
    .modal-content2 {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 50%;
      border-radius: 20px;
    }
    
    /* The Close Button */
    .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }
    
    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    .display-inline{
     display: inline;
      overflow:auto;
    }
  
    .header img {
      float: left;
  
    }
  
    .header h1 {
    float: left;
  
    }
    .padingInPayFee{
      padding-left: 10%;
    }
  
  </style>


<div class="container-fluid">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Danh Sách Loại Dịch Vụ</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">

      <!-- model-->
      <!--model Chi Tiết Lớp --> 
      <div id="ServiceTypeDetail" class="modal">
          <!-- Modal content -->
          <div class="modal-content" id="ServiceTypeDetailContent">
            <div class="form-group">
                <div>
                    <h6 class="font-weight-bold text-primary">Tên Loại Dịch Vụ Dự Án:</h6>
                </div>
                <input id="idInputServiceTypeName" type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án...">
            </div>
            <input type="hidden" id="idInputServiceType" name="id">
            <button type="button" onclick="updateCategory()" class="btn btn-info btn-icon-split">
                <span class="text">Cập Nhật</span>
            </button>
          </div>
      </div>
        <!--end model Chi Tiết Lớp -->

      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Danh Sách Loại Dịch Vụ</h6>
      </div>
      <div class="card-body">
        <!-- will print here-->
        <div class="table-responsive" id="printHere">
            <div class="input-group">
                <input type="text" data-table="order-table" class="form-control bg-light border-0 small light-table-filter" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
          <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
              <tr align="center">
                <th>STT</th>
                <th>Tên Loại Dịch Vụ</th>
                <th>Chỉnh Sửa</th>
              </tr>
            </thead>
            <tbody align="center">
              @foreach ($serviceTypes as $key=>$serviceType)
              <tr align="center" >
                  <td>{{$key+1}}</td>
                  <td>{{$serviceType->name}}</td>
                  <td>
                    <div class="form-group">
                      <a href="{{route('getAdminInsertPriceAndServicePage',$serviceType->id)}}">
                        <button  type="button" class="btn btn-info btn-icon-split">
                          <span class="text">Thêm Bảng Giá Và Dịch Vụ</span>
                        </button>
                      </a>
                      
                      <button type="button" onclick="getDetailOfServiceType({{$serviceType->id}})" class="btn btn-warning btn-icon-split">
                        <span class="text">Chỉnh Sửa</span>
                      </button>
                      <button onclick="deleteServiceType({{$serviceType->id}},this)" type="button" class="btn btn-danger btn-icon-split">
                        <span class="text">Xóa</span>
                      </button>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>

@endsection


@section('script')
  <script>
      (function(document) {
      'use strict';
      var LightTableFilter = (function(Arr) {
        var _input;
        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }
        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }
        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);
      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });
    })(document);
  </script>

<script>
  function deleteServiceType(idServiceType,element){
    $.get("xoa-loai-dich-vu/"+idServiceType,function(data,status){
      if(data.status=="success"){
        showSuccessMessage("Xóa Thành Công");
        element.parentNode.parentNode.parentNode.remove();
        closeMessage(5000,"messageSuccess"); 
      }else{
        if(data.status=="false"){
          showErrorMessage("Không Thể Xóa Vì Có Dịch Vụ Khác Thuộc Loại Dịch Vụ Này");
          closeMessage(5000,"messageError"); 
        }
      }
    });
  }

  function showSuccessMessage(message){
    document.getElementById("messageSuccessData").innerHTML=message;
    document.getElementById("messageSuccess").style.display="block";
  }

  function showErrorMessage(message){
    document.getElementById("messageErorData").innerHTML=message;
    document.getElementById("messageError").style.display="block";
  }

  function closeMessage(time,id){
    setTimeout(function(){
      document.getElementById(id).style.display="none";
    },time)
  }1
</script>

<script>

  function getDetailOfServiceType(idServiceType){
    $.get("thong-tin-loai-dich-vu/"+idServiceType,function(data,status){
      if(status=="success"){
        document.getElementById("idInputServiceType").value=idServiceType;
        document.getElementById("idInputServiceTypeName").value=data.serviceTypeDetail.name;
        showModel();
      }
    });
  }

  function updateCategory(){
    if(validateCategoryBeforSubmit()==false){
      alert("Vui Lòng Nhập Tên Loại Dịch Vụ");
      return;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.post("cap-nhat-loai-dich-vu",{
      _token: CSRF_TOKEN,
      name:document.getElementById("idInputServiceTypeName").value,
      id:document.getElementById("idInputServiceType").value
    },function(data,status){
      if(status=="success"){
        closeModel();
        location.reload();
      }
    });
  }

  function closeModel(){
    var modal = document.getElementById('ServiceTypeDetail');
    modal.style.display = "none";
  }
  function showModel(){
    var modal = document.getElementById('ServiceTypeDetail');
    modal.style.display = "block";
  }


  window.onclick = function(event) {
    var modal = document.getElementById('ServiceTypeDetail');
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }


  function validateCategoryBeforSubmit(){
    if(document.getElementById("idInputServiceTypeName").value==""){
      return false;
    }
    return true;
  }
</script>
 
@endsection