@extends('admin.layout.index')



@section('content')
<style>
    body {font-family: Arial, Helvetica, sans-serif;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        border-radius: 20px;
    }
    .modal-content2 {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
        border-radius: 20px;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .display-inline{
        display: inline;
        overflow:auto;
    }

    .header img {
        float: left;

    }

    .header h1 {
    float: left;

    }
    .padingInPayFee{
        padding-left: 10%;
    }
</style>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <!--model thêm dịch vụ --> 
    <div id="modalAddService" class="modal">
        <!-- Modal content -->
        <div class="modal-content" id="ServiceTypeDetailContent">
            <div class="form-group">
                <div>
                    <h6 class="font-weight-bold text-primary">Tên Dịch Vụ:</h6>
                </div>
                <input id="idInputServiceName" type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Dịch Vụ...">
            </div>
            <input type="hidden" id="idInputServiceType" name="id">
            <button type="button" onclick="addService()" class="btn btn-info btn-icon-split">
                <span class="text">Thêm Dịch Vụ</span>
            </button>
        </div>
    </div>
    <!--end model thêm dịch vụ -->
    <!--model Chi Tiết Lớp --> 
    <div id="modalAddPrice" class="modal">
        <!-- Modal content -->
        <div class="modal-content" id="ServiceTypeDetailContent">
            <div class="form-group">
                <div>
                    <h6 class="font-weight-bold text-primary">Tên Bảng Giá:</h6>
                </div>
                <input id="idInputPriceName" type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Bảng Giá...">
            </div>
            <div class="form-group">
              <div>
                  <h6 class="font-weight-bold text-primary">Giá:</h6>
              </div>
              <input id="idInputPriceCost" type="number" name="cost" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án...">
            </div>
            <input type="hidden" id="idInputServiceType" name="id">
            <button type="button" onclick="addPrice()" class="btn btn-info btn-icon-split">
                <span class="text">Cập Thêm Bảng Giá</span>
            </button>
        </div>
    </div>
<!--end model Chi Tiết Lớp -->


    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Danh Sách Dịch Vụ Và Bản Giá Của Loại Dịch Vụ {{$serviceType->name}}</h1>
    </div>
    <input type="hidden" name="idServiceType" id="idServiceType" value="{{$serviceType->id}}">
    <form onsubmit="return validateProjectFormBeForSubmit()" name="projectForm" class="user" action="{{route('adminPostAddProject')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Danh Sách Dịch Vụ</h6>
        </div>
        <div class="card-body">
        <!-- will print here-->
        <div class="table-responsive" id="printHere">
            <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                <th>STT</th>
                <th>Tên Dịch Vụ</th>
                <th>Chỉnh Sửa</th>
                </tr>
            </thead>
            <tbody align="center">
                @foreach ($services as $key=>$service)
                <tr align="center" >
                    <td>{{$key+1}}</td>
                    <td>{{$service->name}}</td>
                    <td>
                        <div class="form-group">
                            <button onclick="deleteService({{$service->id}},this)" type="button" class="btn btn-danger btn-icon-split">
                                <span class="text">Xóa</span>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
            <div class="col text-center">
                <button type="button" onclick="showAddServiceModal()" class="btn btn-primary col-lg-4">
                        Thêm Dịch Vụ
                </button>
            </div>
        </div>

        <hr>

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Danh Sách Bảng Giá</h6>
        </div>
        <div class="card-body">
        <!-- will print here-->
        <div class="table-responsive" id="printHere">
            <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                <th>STT</th>
                <th>Tên Bảng Giá</th>
                <th>Giá</th>
                <th>Chỉnh Sửa</th>
                </tr>
            </thead>
            <tbody align="center">
                @foreach ($prices as $key=>$price)
                <tr align="center" >
                    <td>{{$key+1}}</td>
                    <td>{{$price->name}}</td>
                    <td>{{$price->cost}}</td>
                    <td>
                        <div class="form-group">
                            <a href="{{route('getAdminInsertPriceAndServicePage',$service->id)}}">
                                <button  type="button" class="btn btn-info btn-icon-split">
                                    <span class="text">Thêm Bảng Giá Và Dịch Vụ</span>
                                </button>
                            </a>
                            <button onclick="deletePrice({{$price->id}},this)" type="button" class="btn btn-danger btn-icon-split">
                                <span class="text">Xóa</span>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
            <div class="col text-center">
                <button type="button" onclick="showModalAddPrice()" class="btn btn-primary col-lg-4">
                        Thêm Bảng Giá
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>
@endsection

@section('script')


<script>
  function showAddServiceModal(){
      showModalAddService();
  }

  function deleteService(idService,element){
    $.get("xoa-dich-vu/"+idService,function(data,status){
      if(data.status=="success"){
        showSuccessMessage("Xóa Thành Công");
        element.parentNode.parentNode.parentNode.remove();
        closeMessage(5000,"messageSuccess"); 
      }else{
        if(data.status=="false"){
          showErrorMessage("Không Thể Xóa");
          closeMessage(5000,"messageError"); 
        }
      }
    });
  }

  function addService(){
    if(validateAddServiceBeforeSubmit()==false){
      alert("Vui Lòng Nhập Tên Dịch Vụ");
      return;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.post("them-dich-vu",{
      _token: CSRF_TOKEN,
      name:document.getElementById("idInputServiceName").value,
      idServiceType:document.getElementById("idServiceType").value
    },function(data,status){
      if(status=="success"){
        closeAddServiceModal();
        location.reload();
      }
    });
  }

  function addPrice(){
    if(validateAddPriceBeforeSubmit()==false){
      alert("Vui Lòng Nhập Tên Dịch Vụ");
      return;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.post("them-bang-gia",{
      _token: CSRF_TOKEN,
      name:document.getElementById("idInputPriceName").value,
      cost:document.getElementById("idInputPriceCost").value,
      idServiceType:document.getElementById("idServiceType").value
    },function(data,status){
      if(status=="success"){
        closeAddPriceModal();
        location.reload();
      }
    });
  }

  function deletePrice(idPrice,element){
    $.get("xoa-bang-gia/"+idPrice,function(data,status){
      if(data.status=="success"){
        showSuccessMessage("Xóa Thành Công");
        element.parentNode.parentNode.parentNode.remove();
        closeMessage(5000,"messageSuccess"); 
      }else{
        if(data.status=="false"){
          showErrorMessage("Không Thể Xóa");
          closeMessage(5000,"messageError"); 
        }
      }
    });
  }

  function showSuccessMessage(message){
    document.getElementById("messageSuccessData").innerHTML=message;
    document.getElementById("messageSuccess").style.display="block";
  }

  function showErrorMessage(message){
    document.getElementById("messageErorData").innerHTML=message;
    document.getElementById("messageError").style.display="block";
  }

  function closeMessage(time,id){
    setTimeout(function(){
      document.getElementById(id).style.display="none";
    },time)
  }

  

  function closeAddServiceModal(){
    var modal = document.getElementById('modalAddService');
    modal.style.display = "none";
  }

  function showModalAddService(){
    var modal = document.getElementById('modalAddService');
    modal.style.display = "block";
  }

  function showModalAddPrice(){
    var modal= document.getElementById('modalAddPrice');
    modal.style.display = "block";
  }
  function closeAddPriceModal(){
    var modal = document.getElementById('modalAddService');
    modal.style.display = "none";
  }

  window.onclick = function(event) {
    var modal = document.getElementById('modalAddPrice');
    if (event.target == modal) {
      modal.style.display = "none";
    }
    var modal2 = document.getElementById('modalAddService');
    if (event.target == modal2) {
      modal2.style.display = "none";
    }
  }

  

  function validateAddServiceBeforeSubmit(){
    if(document.getElementById("idInputServiceName").value==""){
      return false;
    }
    return true;
  }

  function validateAddPriceBeforeSubmit(){
    if(document.getElementById("idInputPriceName").value==""){
      return false;
    }
    if(document.getElementById("idInputPriceCost").value==""){
      return false;
    }
    return true;
  }
</script>
 
@endsection


