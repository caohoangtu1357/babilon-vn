@extends('admin.layout.index')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Chỉnh Sửa Dự Án</h1>
    </div>
    <form onsubmit="return validateProjectFormBeForSubmit()"name="projectForm" class="user" action="{{route('adminPostUpdateProject')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Tên Dự Án:</h6>
            </div>
            <input type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án..." value="{{$projectDetail->name}}">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Loại Dự Án:</h6>
            </div>
            <select name="idProjectType" class="form-control">
                <option disabled selected>Chọn Loại Dự Án</option>
                @foreach ($projectTypes as $projectType)
                    <option value="{{$projectType->id}}" @if ($projectType->id==$projectDetail->idProjectType){{"selected"}} @endif >{{$projectType->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hạng Mục:</h6>
            </div>
            <select name="idCategory" class="form-control">
                <option disabled selected>Chọn Hạng Mục Dự Án</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}" @if ($category->id==$projectDetail->categoryId){{"selected"}} @endif>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Chủ Đầu Tư:</h6>
            </div>
            <input name="customerName" type="text" class="form-control" id="exampleInputEmail" value="{{$projectDetail->customerName}}">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Năm Thực Hiện:</h6>
            </div>
            <input name="startYear" type="number" class="form-control" id="exampleInputEmail" value="{{$projectDetail->startYear}}" min="1000" max="9999">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Địa Chỉ:</h6>
            </div>
            <input name="address" type="text" class="form-control" id="exampleInputEmail" value="{{$projectDetail->address}}">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hình Ảnh Dự Án:</h6>
            </div>
            <input name="imageName[]" type="text" class="form-control imageName" id="exampleInputEmail" placeholder="Tên Hình Ảnh...">
            <input name="imageDescription[]" type="text" class="form-control imageDescription" id="exampleInputEmail" placeholder="Mô Tả Hình Ảnh...">
            <select class="form-control imageType" name="imageType[]">
                <option selected disabled>Chọn Loại Ảnh:</option>
                <option value="1">Hình Đại Diện</option>
                <option value="2">Hình Ảnh Dự Án</option>
                <option value="3">Ảnh Thiết Kế</option>
                <option value="4">Ảnh Sau Thi Công</option>
            </select>
            <input name="imageFile[]" class="form-control imageFile" type="file">
            <button class="btn btn-primary" onclick="duplicate(this)" type="button">Thêm 1 Ảnh Nữa</button>
        </div>

        <div class="table-responsive" id="printHere">
            <div class="input-group">
                <input type="text" data-table="order-table" class="form-control bg-light border-0 small light-table-filter" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
          <table class="table table-bordered order-table table" width="100%" cellspacing="0">
            <thead>
            <tr align="center">
                <th>STT</th>
                <th>Tên Hình Ảnh</th>
                <th>Mô Tả</th>
                <th>Loại Hình Ảnh</th>
                <th>Hình Ảnh</th>
                <th>Xóa</th>
            </tr>
            </thead>
                <tbody align="center">
                    @foreach ($imagesOfProject as $key=>$image)
                        <tr align="center" style="vertical-align:middle" >
                            <td style="vertical-align:middle">{{$key+1}}</td>
                            <td style="vertical-align:middle">{{$image->name}}</td>
                            <td style="vertical-align:middle">{{$image->description}}</td>
                            <td style="vertical-align:middle">{{$imageTypeName[$image->imageType]}}</td>
                            <td style="vertical-align:middle"><img src="{{ URL::asset('upload/imageProject/')}}/{{$image->imageLink}}" width="20%" height="auto"></td>
                            <td style="vertical-align:middle">
                                <div class="form-group">
                                    <button type="button" class="btn btn-danger btn-icon-split col-lg-8" onclick="deleteImage({{$image->id}},this)">
                                        <span class="text">Xóa</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <input type="hidden" name="id" value="{{$projectDetail->id}}">

        <div class="col text-center">
            <button type="submit" onclick="submitAddProjectForm()" class="btn btn-primary col-lg-4">
                    Cập Nhật
            </button>
        </div>
        <hr>
    </form>
    <hr>
  </div>
@endsection

@section('script')
<script>
    function duplicate(id) {
    var i = 0;
    var original = id.parentNode;
    var clone = original.cloneNode(true); 
    original.appendChild(clone);
    }
</script>
<script>
    function deleteImage(idImage,element){
        $.get("xoa-hinh-anh/"+idImage,function(data,status){
            if(data.status=="success"){
                showSuccessMessage("Đã Xóa Thành Công");
                element.parentNode.parentNode.parentNode.remove();
                closeMessage(5000); 
            }
        });
    }

    function showSuccessMessage(message){
      document.getElementById("messageSuccessData").innerHTML=message;
      document.getElementById("messageSuccess").style.display="block";
    }

    function closeMessage(time){
      setTimeout(function(){
        document.getElementById("messageSuccess").style.display="none";
      },time)
    }

</script>


<script>
    function validateProjectFormBeForSubmit(){
        var projectForm=document.forms.projectForm;
        if(projectForm.name.value==""){
            alert("Vui Lòng Kiểm Tra Tên Dự Án");
            return false;
        }
        if(projectForm.idProjectType.value==""){
            alert("Vui Lòng Kiểm Tra Loại Dự Án");
            return false;
        }
        if(projectForm.idCategory.value==""){
            alert("Vui Lòng Kiểm Tra Hạng Mục Dự Án");
            return false;
        }
        if(projectForm.customerName.value==""){
            alert("Vui Lòng Kiểm Tra Chủ Đầu Tư");
            return false;
        }
        if(projectForm.startYear.value==""){
            alert("Vui Lòng Kiểm Tra Năm Bắt Đầu Dự Án");
            return false;
        }
        if(projectForm.address.value==""){
            alert("Vui Lòng Kiểm Tra Địa Chỉ Dự Án");
            return false;
        }
        if(validateImageInfor()==false){
            alert("Vui Lòng Kiểm Tra Lại Thông Tin Hình Ảnh");
            return false;
        }
        return true;

    }

    function validateImageInfor(){
        for(let i=0;i<document.getElementsByClassName("imageName").length;i++){
            if(document.getElementsByClassName('imageName')[i].value!=""){
                if(document.getElementsByClassName('imageName')[i].value==""){
                return false;
                }
                if(document.getElementsByClassName('imageDescription')[i].value==""){
                    return false;
                }
                if(document.getElementsByClassName('imageType')[i].value==""){
                    return false;
                }
                if(document.getElementsByClassName('imageFile')[i].value==""){
                    return false;
                }
            }
            
        }
        return true;
    }
</script>

<script>
    
    

</script>

@endsection

