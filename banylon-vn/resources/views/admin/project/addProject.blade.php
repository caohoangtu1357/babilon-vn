@extends('admin.layout.index')



@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Thêm Dự Án</h1>
    </div>
    <form onsubmit="return validateProjectFormBeForSubmit()" name="projectForm" class="user" action="{{route('adminPostAddProject')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Tên Dự Án:</h6>
            </div>
            <input type="text" name="name" class="form-control" id="exampleInputEmail" placeholder="Tên Dự Án...">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Loại Dự Án:</h6>
            </div>
            <select name="idProjectType" class="form-control">
                <option disabled value="" selected>Chọn Loại Dự Án</option>
                @foreach ($projectTypeList as $projectType)
                    <option value="{{$projectType->id}}">{{$projectType->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hạng Mục:</h6>
            </div>
            <select name="idCategory" class="form-control">
                <option disabled value="" selected>Chọn Hạng Mục Dự Án</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Chủ Đầu Tư:</h6>
            </div>
            <input name="customerName" type="text" class="form-control" id="exampleInputEmail" placeholder="Tên Chủ Đầu Tư...">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Năm Thực Hiện:</h6>
            </div>
            <input name="startYear" type="number" class="form-control" id="exampleInputEmail" placeholder="Năm Thực Hiện..." min="1000" max="9999">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Địa Chỉ:</h6>
            </div>
            <input name="address" type="text" class="form-control" id="exampleInputEmail" placeholder="Địa Chỉ...">
        </div>
        <div class="form-group">
            <div>
                <h6 class="font-weight-bold text-primary">Hình Ảnh Dự Án:</h6>
            </div>
            <input name="imageName[]" type="text" class="form-control imageName" id="exampleInputEmail" placeholder="Tên Hình Ảnh...">
            <input name="imageDescription[]" type="text" class="form-control imageDescription" id="exampleInputEmail" placeholder="Mô Tả Hình Ảnh...">
            <select class="form-control imageType" name="imageType[]">
                <option selected value="" disabled>Chọn Loại Ảnh:</option>
                <option value="1">Hình Đại Diện</option>
                <option value="2">Hình Ảnh Dự Án</option>
                <option value="3">Ảnh Thiết Kế</option>
                <option value="4">Ảnh Sau Thi Công</option>
            </select>
            <input name="imageFile[]" class="form-control imageFile" type="file">
            <button class="btn btn-primary" onclick="duplicate(this)" type="button">Thêm 1 Ảnh Nữa</button>
        </div>
        <div class="col text-center">
            <button type="submit" onclick="submitAddProjectForm()" class="btn btn-primary col-lg-4">
                    Thêm
            </button>
        </div>
        
        <hr>
    </form>
    <hr>
</div>
@endsection

@section('script')
<script>
    function duplicate(id) {
    var i = 0;
    var original = id.parentNode;
    var clone = original.cloneNode(true); 
    original.appendChild(clone);
    }
</script>

<script>
    function validateProjectFormBeForSubmit(){
        var projectForm=document.forms.projectForm;
        if(projectForm.name.value==""){
            alert("Vui Lòng Kiểm Tra Tên Dự Án");
            return false;
        }
        if(projectForm.idProjectType.value==""){
            alert("Vui Lòng Kiểm Tra Loại Dự Án");
            return false;
        }
        if(projectForm.idCategory.value==""){
            alert("Vui Lòng Kiểm Tra Hạng Mục Dự Án");
            return false;
        }
        if(projectForm.customerName.value==""){
            alert("Vui Lòng Kiểm Tra Chủ Đầu Tư");
            return false;
        }
        if(projectForm.startYear.value==""){
            alert("Vui Lòng Kiểm Tra Năm Bắt Đầu Dự Án");
            return false;
        }
        if(projectForm.address.value==""){
            alert("Vui Lòng Kiểm Tra Địa Chỉ Dự Án");
            return false;
        }
        if(validateImageInfor()==false){
            alert("Vui Lòng Kiểm Tra Lại Thông Tin Hình Ảnh");
            return false;
        }
        return true;

    }

    function validateImageInfor(){
        for(let i=0;i<document.getElementsByClassName("imageName").length;i++){
            if(document.getElementsByClassName('imageName')[i].value==""){
                return false;
            }
            if(document.getElementsByClassName('imageDescription')[i].value==""){
                return false;
            }
            if(document.getElementsByClassName('imageType')[i].value==""){
                return false;
            }
            if(document.getElementsByClassName('imageFile')[i].value==""){
                return false;
            }
        }
        return true;
    }
</script>
@endsection

