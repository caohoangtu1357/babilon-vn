<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "user admin",
            'email' => "admin@gmail.com",
            'password' => bcrypt('12345678'),
            'role'=>"admin"
        ]);
    }
}
