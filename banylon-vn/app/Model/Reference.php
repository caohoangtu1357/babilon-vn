<?php

namespace App\Model;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;
use DB;
class Reference extends BaseModel
{
    protected $table='reference';
    public $primaryKey='id';
    public $fillable = [
        'ngayDang', 'title', 'content','imageLink','imageDescription'
    ];

    static public function insertReference($title,$content,$imageDescription,$image){
        $imageLink=self::saveImage("upload/imageReference/",$image);
        DB::table('reference')->insert(['title'=>$title,'content'=>$content,'imageLink'=>$imageLink,'ngayDang'=>date('Y-m-d'),'imageDescription'=>$imageDescription]);
    }

    static public function getReferences(){
        return DB::table('reference')->get();
    }

    static public function getReferenceInforById($idReference){
        return DB::table('reference')->where('id',$idReference)->first();
    }

    static public function deleteReferenceById($idReference){
        DB::table('reference')->where('id',$idReference)->delete();
    }

    static public function updateReference($id,$title,$content,$imageDescription,$image){
        if($image!=null){
            $imageLink=self::saveImage("upload/imageReference/",$image);
            DB::table('reference')->where('id',$id)->update(['title'=>$title,'content'=>$content,'imageDescription'=>$imageDescription,'imageLink'=>$imageLink]);
        }else{
            DB::table('reference')->where('id',$id)->update(['title'=>$title,'content'=>$content,'imageDescription'=>$imageDescription]);
        }
    }

    
}
