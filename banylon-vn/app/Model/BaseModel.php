<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public static function saveImage($destinationPath,$file){
        //$file=$request->file('projectAvatar');
        $name=$file->getClientOriginalName();
        $photo=str_random(7)."_".$name;
        while(file_exists($destinationPath.$photo)){
            $photo=str_random(7)."_".$name;
        }
        $file->move($destinationPath,$photo);
        
        return $photo ;//new image name
    }

}
