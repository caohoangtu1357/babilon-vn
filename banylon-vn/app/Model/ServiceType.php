<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class ServiceType extends Model
{
    protected $table='service_type';
    public $primaryKey='id';
    public $fillable = [
        'name',
    ];

    static public function insertServiceType($name){
        DB::table('service_type')->insert(['name'=>$name]);
    }

    static public function getServiceTypes(){
        return DB::table('service_type');
    }

    static public function getServiceTypeDetailById($idServiceType){
        return DB::table('service_type')->where('id',$idServiceType);
    }

    static public function updateServiceTypeById($idServiceType,$name){
        return DB::table('service_type')->where('id',$idServiceType)->update(['name'=>$name]);
    }

    static public function deleteServiceTypeyId($idServiceType){
        return DB::table('service_type')->where('id',$idServiceType)->delete();
    }

    public function service(){
        return $this->hasMany('App\Model\Service','idServiceType','id');
    }

    public function price(){
        return $this->hasMany('App\Model\Price','idServiceType','id');
    }
}
