<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Service extends Model
{
    protected $table='service';
    public $primaryKey='id';
    public $fillable = [
        'name', 'idServiceType',
    ];

    static public function getServicesByServiceTypeId($idServiceType){
        return DB::table('service')->where('idServiceType',$idServiceType);
    }

    static public function addService($name,$idServiceType){
        return DB::table('service')->insert(['name'=>$name,'idServiceType'=>$idServiceType]);
    }

    static public function deleteServiceById($idService){
        return DB::table('service')->where('id',$idService)->delete();
    }
}
