<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Services\SlugService;
use DB;


class Project extends Model
{
    use Sluggable;

    protected $table='project';
    public $primaryKey='id';
    public $fillable = [
        'name', 'customerName', 'idProjectType','address','startYear','idCategory','slug'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    static public function addProject($name,$customerName,$idProjectType,$address,$startYear,$idCategory){
        $slug=SlugService::createSlug(Project::class,'slug',$name);
        return DB::table('project')->insertGetId(['name'=>$name,'customerName'=>$customerName,'idProjectType'=>$idProjectType,'address'=>$address,'startYear'=>$startYear,'idCategory'=>$idCategory,'slug'=>$slug]);
    }

    static public function projectList(){
        return DB::table('project')
                    ->join('project_type','project_type.id','=','project.idProjectType')
                    ->select(['project_type.name as projectTypeName','project.name as projectName','project.startYear','project.customerName','project.address','project.id as id','project.slug as slug']);
    }

    static public function getProjectDetailById($idProject){
        return DB::table('project')
                    ->select(['project.id as id','project_type.name as projectTypeName','project.name as name','project.address','project.customerName','project.startYear','category.name as categoryName','category.id as categoryId','project_type.id as idProjectType','project.slug as slug'])
                    ->join('project_type','project_type.id','=','project.idProjectType')
                    ->join('category','category.id','=','project.idCategory')
                    ->where('project.id',$idProject);
    }

    static public function thumbnailEachProject($num){
        if($num=="all"){
            return DB::table('project')
                    ->select(['project_image.imageLink','project.name as projectName','project.id as id','project_image.description as description','project.slug as slug'])
                    ->join('project_image',"project.id","=","project_image.idProject")
                    ->where('project_image.imageType','=',1);
        }
        return DB::table('project')
                    ->select(['project_image.imageLink','project.name as projectName','project.id as id','project_image.description as description','project.slug as slug'])
                    ->join('project_image',"project.id","=","project_image.idProject")
                    ->where('project_image.imageType','=',1)
                    ->limit($num);
    }

    public function thumbnail(){
        $this->hasMany('App\Model\ImageProject','idProject','id')->where('project_image.imageType',1);
    }

    static public function deleteProjectDetail($id){
        return DB::table('project')->where('id',$id)->delete();
    }

    static public function updateProjectDetail($details){
        DB::table('project')->where('id',$details['id'])->update($details);
    }

    static public function findIdBySlug($slug){
        $project= DB::table('project')->where('slug',$slug)->first();
        return $project->id;
    }

}
