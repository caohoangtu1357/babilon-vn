<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\BaseModel;
use DB;
class Slide extends BaseModel
{
    protected $table='slide';
    public $primaryKey='id';
    public $fillable = [
        'description', 'imageLink'
    ];

    static public function getSlides(){
        return DB::table('slide')->get();
    }

    static public function addImage($description,$imageFile){
        $imageLink=self::saveImage("upload/imageSlide/",$imageFile);
        DB::table('slide')->insert(['description'=>$description,'imageLink'=>$imageLink]);
    }

    static public function imageDetail($idImage){
        return DB::table('slide')->where('id',$idImage)->first();
    }

    static public function deleteImage($idImage){
        DB::table('slide')->where('id',$idImage)->delete();
    }

}
