<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\BaseModel;
use DB;
class ProjectImage extends BaseModel
{
    protected $table='project_image';
    public $primaryKey='id';
    public $fillable = [
        'name', 'description', 'imageType','idProject','imageLink'
    ];

    static public function insertImageProject($name,$description,$imageType,$idProject,$imageFile){
        $imageLink=self::saveImage("upload/imageProject/",$imageFile);
        return DB::table('project_image')->insertGetId(['name'=>$name,'description'=>$description,'imageType'=>$imageType,'idProject'=>$idProject,'imageLink'=>$imageLink]);
    }

    static public function getAllImageByIdProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject);
    }

    static public function getThumbnailOfProjectByIdProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject)->where('imageType',1);
    }
    static public function getImageOfProjectByIdProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject)->where('imageType',2);
    }

    static public function getDesignImageOfProjectByIdProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject)->where('imageType',3);
    }

    static public function getImageAfterConstructByIdProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject)->where('imageType',4);
    }

    static public function getAllImageOfProject($idProject){
        return DB::table('project_image')->where('idProject',$idProject);
    }

    static public function deleteImageByIdProject($idProject){
        return DB::Table('project_image')->where('idProject',$idProject)->delete();
    }

    static public function deleteImageById($idImage){
        return DB::table('project_image')->where('id',$idImage)->delete();
    }

    static public function getImageById($idImage){
        return DB::Table('project_image')->where('id',$idImage);
    }
}
