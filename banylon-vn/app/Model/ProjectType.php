<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectType extends Model
{
    use Sluggable;
    protected $table='project_type';
    public $primaryKey='id';
    public $fillable = [
        'name','slug'
    ];


    /**
     * 
     * slug
     * 
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    static public function addProjectType($name){
        $slug=SlugService::createSlug(ProjectType::class,'slug',$name);
        return DB::table('project_type')->insert(['name'=>$name,'slug'=>$slug]);
    }

    static public function getProjectTypeList(){
        return DB::table('project_type');
    }

    static public function projectTypeListWithTotalProject(){
        return DB::table('project_type')
                ->select(['project_type.name as name','project_type.id as id',DB::raw('count(project.name) as total')])
                ->leftJoin('project','project.idProjectType','=','project_type.id')
                ->groupBy(['project_type.name','project_type.id']);
    }

    static public function getProjectByProjectTypeId($idProjectType){
        return DB::table('project_type')
                        ->select(['project.name as projectName','project.slug as slug','project.id as idProject','project_image.name as imageName','project_image.imageLink as imageLink','project_image.description as imageDescription'])
                        ->join('project','project.idProjectType','=','project_type.id')
                        ->join('project_image','project.id','=','project_image.idProject')
                        ->where('project_image.imageType','=',1)
                        ->where('project_type.id',$idProjectType);
    }

    public function project($idProjectType){
        return $this->hasMany('App\Model\Project','idProjectType','id');
    }

    static public function isHaveProjectBelong($idProjectType){
        return DB::table('project_type')
                        ->join('project','project_type.id','=','project.idProjectType')
                        ->first();
    }

    static public function deleteProjectType($idProjectType){
        return DB::table('project_type')->where('id',$idProjectType)->delete();
    }

    static public function updateProjectTypeById($idProjectType,$projectTypeName){
        return DB::table('project_type')->where('id',$idProjectType)->update(['name'=>$projectTypeName]);
    }

    static public function getProjectTypeIdBySlug($slug){
        $projectType=DB::table('project_type')->where('slug',$slug)->first();
        return $projectType->id;
    }

    

}
