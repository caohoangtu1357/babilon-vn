<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class ViewNumber extends Model
{
    protected $table='view_number';
    public $primaryKey='id';
    public $fillable = [
        'date', 'number'
    ];

    static public function increaseViewNumber(){
        if(DB::table('view_number')->where('date',Date('Y-m-d'))->first()){
            DB::table('view_number')->where('date',Date('Y-m-d'))->increment('number',1);
        }else{
            DB::table('view_number')->insert(['date'=>Date('Y-m-d'),'number'=>1]);
        }
    }

    static public function getNumViewByDayInMonth($month){
        return DB::table('view_number')->whereMonth('date',$month)->get();
    }

    static public function getNumViewEachMonthByYear($year){
        return DB::table('view_number')
                ->select([DB::raw("DATE_FORMAT(date, '%m') as month"), DB::raw("sum(number) as number")])
                ->whereYear('date',$year)
                ->groupBy('month')
                ->get();
    }
}
