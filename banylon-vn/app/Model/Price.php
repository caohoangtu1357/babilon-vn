<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Price extends Model
{
    protected $table='price';
    public $primaryKey='id';
    public $fillable = [
        'name', 'cost', 'idServiceType'
    ];

    static public function getPricesByIdServiceType($idServiceType){
       return DB::table('price')->where('idServiceType',$idServiceType);
    }

    static public function addPrice($name,$cost,$idServiceType){
        return DB::table('price')->insert(['name'=>$name,'cost'=>$cost,'idServiceType'=>$idServiceType]);
    }

    static public function deletePrice($id){
        return DB::table('price')->where('id',$id)->delete();
    }
}
