<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends BaseModel
{
    protected $table='category';
    public $fillable = [
        'name'
    ];

    static public function getCategories(){
        return DB::table('category');
    }

    static public function addCategory($name){
        return DB::table("category")->insert(["name"=>$name]);
    }

    static function categoryListWithTotalProject(){
        return DB::table('category')
                ->select(['category.name as name','category.id as id',DB::raw('count(project.name) as total')])
                ->leftJoin('project','project.idCategory','=','category.id')
                ->groupBy('category.name');
    }

    static public function isHaveProjectBelong($idCategory){
        return DB::table('category')
                ->join('project','project.idCategory','=','category.id')
                ->where('category.id',$idCategory)
                ->first();
    }

    static public function deleteCategory($idCategory){
        return DB::table('category')
                        ->where('id',$idCategory)
                        ->delete();
    }

    static public function updateCategoryById($idCategory,$dataArray){
        return DB::Table('category')->where('id',$idCategory)->update($dataArray);
    }

    
}
