<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferenceImage extends BaseModel
{
    protected $table='reference_image';
}
