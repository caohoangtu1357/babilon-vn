<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use DB;
use App\Model\ProjectType;
use App\Model\Slide;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $slides=Slide::getSlides();
        $projectTypeList=ProjectType::getProjectTypeList()->get();
        View::share('projectTypeList', $projectTypeList);
        View::share('slides', $slides);
        Schema::defaultStringLength(150);
    }
}
