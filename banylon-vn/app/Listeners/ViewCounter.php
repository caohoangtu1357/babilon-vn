<?php

namespace App\Listeners;

use App\Events\IncreaseView;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\ViewNumber;
use Illuminate\Session\Store;
class ViewCounter
{
    private $session;
    private $idPage;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Store $session)
    {
        $this->session=$session;
    }

    /**
     * Handle the event.
     *
     * @param  IncreaseView  $event
     * @return void
     */
    public function handle(IncreaseView $event)
    {
        $this->idPage=$event->getIdPage();
      
        if(!$this->isPageViewed($this->idPage)){
            ViewNumber::increaseViewNumber();
            $this->storeViewedPage($this->idPage);
        }
    }

    public function isPageViewed($idPage){
        $viewed = $this->session->get('viewedPage',[]);
        return array_key_exists($idPage, $viewed);
    }

    public function storeViewedPage($idPage){
        $key="viewedPage." .$idPage;
        $this->session->put($key,time());
    }
}
