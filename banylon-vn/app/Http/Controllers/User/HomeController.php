<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Events\IncreaseView;
use App\Model\ServiceType;

class HomeController extends Controller
{
    public function getHomePage(){
        $this->increaseView(0);
        $thumbnailEachProject=Project::thumbnailEachProject(12)->get();
        $serviceTypes=$this->getServiceType();
        return view("user.userMainPage",['thumbnailEachProject'=>$thumbnailEachProject,'serviceTypes'=>$serviceTypes]);
    }

    public function increaseView($idHomePage){
        event(new IncreaseView($idHomePage));
    }

    public function getServiceType(){
        $serviceType =ServiceType::all();
        return $serviceType;
    }
}
