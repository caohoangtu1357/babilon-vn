<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\ProjectImage;
use App\Model\ProjectType;
use App\Events\IncreaseView;

class ProjectController extends Controller
{
    public function getAllProject(){
        $thumbnailEachProject=Project::thumbnailEachProject("all")->get();
        $projectTypes=ProjectType::getProjectTypeList()->get();
        return view("user.project.allProject",['thumbnailEachProject'=>$thumbnailEachProject,'projectTypes'=>$projectTypes]);
    }
    
    public function getProjectDetail($slug){

        $idProject=Project::findIdBySlug($slug);

        $this->increaseView($idProject);
        $projectDetail= Project::getProjectDetailById($idProject)->first();
        $allImage=ProjectImage::getAllImageByIdProject($idProject)->get();
        $projectThumbnail=ProjectImage::getThumbnailOfProjectByIdProject($idProject)->first();
        $projectImages=ProjectImage::getImageOfProjectByIdProject($idProject)->get();
        $projectDesignImage=ProjectImage::getDesignImageOfProjectByIdProject($idProject)->get();
        $projectAfterConstructImage=ProjectImage::getImageAfterConstructByIdProject($idProject)->get();
        return view('user.project.projectDetail',['projectDetail'=>$projectDetail,
                                                  'projectThumbnail'=>$projectThumbnail,
                                                  'projectImages'=>$projectImages,
                                                  'projectDesignImage'=>$projectDesignImage,
                                                  'projectAfterConstructImage'=>$projectAfterConstructImage,
                                                  'allImage'=>$allImage
                                                  ]);
    }

    public function increaseView($idProject){
        event(new IncreaseView($idProject));
    }
}
