<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Reference;
class ReferenceController extends Controller
{
    public function getReferencePage(){
        $references=Reference::getReferences();
        return view('user.reference.reference',['references'=>$references]);
    }
}
