<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
use App\Model\Project;

class ProjectTypeController extends Controller
{
    public function getProjectByProjectTypeId($slug){
        $idProjectType=ProjectType::getProjectTypeIdBySlug($slug);
        $projectTypes=ProjectType::getProjectTypeList()->get();
        $projects=ProjectType::getProjectByProjectTypeId($idProjectType)->get();
        return view('user.projectType.projectOfProjectType',['projects'=>$projects,'projectTypes'=>$projectTypes]);
    }
}
