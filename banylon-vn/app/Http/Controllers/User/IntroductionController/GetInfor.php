<?php

namespace App\Http\Controllers\User\IntroductionController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetInfor extends Controller
{
    public function getIntroducePage(){
        return view('user.introduction.introduction');
    }
}
