<?php

namespace App\Http\Controllers\Admin\ServiceTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceType;
class Update extends Controller
{
    public function postUpdateServiceType(Request $request){
        $this->updateServiceTypeById($request->id,$request->name);
        return response()->json(['status'=>"success"]);
    }

    public function updateServiceTypeById($idServiceType,$name){
        return ServiceType::updateServiceTypeById($idServiceType,$name);
    }
}
