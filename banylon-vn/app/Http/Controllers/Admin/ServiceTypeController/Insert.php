<?php

namespace App\Http\Controllers\Admin\ServiceTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceType;
use App\Model\Service;
use App\Model\Price;
class Insert extends Controller
{
    public function getInsertServiceTypePage(){
        return view('admin.serviceType.addServiceType');
    }

    public function postInsertServiceType(Request $request){
        $this->insertServiceType($request->name);
        return response()->json(['status'=>"success"]);
    }

    
    public function insertServiceType($nameServiceType){
        return ServiceType::insertServiceType($nameServiceType);
    }

    public function getInsertPriceAndServicePage($idServiceType){
        $serviceType=ServiceType::getServiceTypeDetailById($idServiceType)->first();
        $prices=$this->getPricesByIdServiceType($idServiceType);
        $services=$this->getServicesByServiceTypeId($idServiceType);
        return view('admin.serviceType.addServicesAndPrices',['prices'=>$prices,'services'=>$services,'serviceType'=>$serviceType]);
    }

    public function getPricesByIdServiceType($idServiceType){
        return Price::getPricesByIdServiceType($idServiceType)->get();
    }

    public function getServicesByServiceTypeId($idServiceType){
        return Service::getServicesByServiceTypeId($idServiceType)->get();
    }

    public function postInsertServicesAndPrices(Request $request){

    }

    public function insertService($idServiceType){

    }

    public function insertPrice($idServiceType){

    }

    
}
