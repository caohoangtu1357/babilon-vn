<?php

namespace App\Http\Controllers\Admin\ServiceTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceType;
class Delete extends Controller
{
    public function getDeleteServiceType($idServiceType){
        $this->deleteServiceTypeById($idServiceType);
        return response()->json(['status'=>"success"]);
    }

    public function deleteServiceTypeById($idServiceType){
        ServiceType::deleteServiceTypeyId($idServiceType);
    }
}
