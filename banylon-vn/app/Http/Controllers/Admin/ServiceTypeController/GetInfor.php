<?php

namespace App\Http\Controllers\Admin\ServiceTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceType;
class GetInfor extends Controller
{
    public function getServiceTypeList(){
        $serviceTypes= ServiceType::getServiceTypes()->get();
        return view('admin.serviceType.serviceTypes',['serviceTypes'=>$serviceTypes]);
    }

    public function getServiceTypeDetail($idServiceType){
        $serviceTypeDetail=$this->getServiceTypeDetailById($idServiceType);
        return response()->json(['serviceTypeDetail'=>$serviceTypeDetail,"status"=>"success"]);
    }

    public function getServiceTypeDetailById($idServiceType){
        return ServiceType::getServiceTypeDetailById($idServiceType)->first();
    }
}
