<?php

namespace App\Http\Controllers\Admin\ReferenceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Reference;
class Insert extends Controller
{
    public function getInsert(){
        return view('admin.reference.addReference');
    }
    public function postInsert(Request $request){
        $this->insertReference($request->title,$request->content,$request->imageDescription,$request->file('imageFile'));
        return redirect()->back()->with('success',"Thêm Bài Tham Khảo Thành Công");
    }
    
    public function insertReference($title,$content,$imageDescription,$imageFile){
        Reference::insertReference($title,$content,$imageDescription,$imageFile);
    }
}
