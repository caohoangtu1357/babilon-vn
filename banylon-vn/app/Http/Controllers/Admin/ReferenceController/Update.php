<?php

namespace App\Http\Controllers\Admin\ReferenceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Reference;
use File;
class Update extends Controller
{
    private $path="upload/imageReference/";

    public function postUpdateReference(Request $request){
        $referenceDetail=Reference::getReferenceInforById($request->id);
        if($request->file('imageFile')!=null){
            $this->updateReference($request->id,$request->title,$request->content,$request->imageDescription,$request->file('imageFile'));
            $this->deleteOldImageInfolder($referenceDetail->imageLink);
        }else{
            $this->updateReference($request->id,$request->title,$request->content,$request->oldImageDescription,$request->file('imageFile'));
        }
     
        return redirect()->route('getAdminReferences');
    }

    public function updateReference($id,$title,$content,$imageDescription,$imageFile){
        Reference::updateReference($id,$title,$content,$imageDescription,$imageFile);
    }

    public function deleteOldImageInfolder($imageLink){
        if($imageLink!=null){
            File::delete($this->path.$imageLink);
        }
    }
}
