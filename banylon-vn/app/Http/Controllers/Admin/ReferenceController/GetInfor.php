<?php

namespace App\Http\Controllers\Admin\ReferenceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Reference;
class GetInfor extends Controller
{
    public function getReferences(){
        $references=Reference::getReferences();
        return view('admin.reference.references',['references'=>$references]);
    }

    public function getReferenceDetail($idReference){
        $referenceDetail=Reference::getReferenceInforById($idReference);
        return view('admin.reference.referenceDetail',['reference'=>$referenceDetail]);
    }

}
