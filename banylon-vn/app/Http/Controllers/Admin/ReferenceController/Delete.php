<?php

namespace App\Http\Controllers\Admin\ReferenceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Reference;
use File;
class Delete extends Controller
{
    private $path="upload/imageReference/";

    public function getDeleteReference($idReference){
        $referenceDetail=$this->getReferenceInfor($idReference);
        $this->deleteReference($referenceDetail->id);
        $this->deleteReferenceImageInUpLoadFolder($referenceDetail->imageLink);
        return response()->json(['status'=>'success']);
    }

    public function getReferenceInfor($idReference){
        return Reference::getReferenceInforById($idReference);
    }
    public function deleteReference($idReference){
        Reference::deleteReferenceById($idReference);
    }
    public function deleteReferenceImageInUpLoadFolder($imageLink){
        File::delete($this->path.$imageLink);
    }

}
