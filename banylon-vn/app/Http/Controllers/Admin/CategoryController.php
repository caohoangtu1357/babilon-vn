<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
class CategoryController extends Controller
{
    public function getAddCategory(){
        return view('admin.category.addCategory');
    }
    public function postAddCategory(Request $request){
        Category::addCategory($request->name);
        return response()->json(["status"=>"success"]);
    }
    public function getCategoryList(){
        $categories=Category::categoryListWithTotalProject()->get();
        return view('admin.category.categoryList',["categories"=>$categories]);
    }
}
