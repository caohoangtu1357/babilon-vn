<?php

namespace App\Http\Controllers\Admin\PriceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Price;
class Insert extends Controller
{
    public function postAddPrice(Request $request){
        $this->addPrices($request->name,$request->cost,$request->idServiceType);
        return response()->json(['status'=>'success']);
    }

    public function addPrices($name,$cost,$idServiceType){
        return Price::addPrice($name,$cost,$idServiceType);
    }

}
