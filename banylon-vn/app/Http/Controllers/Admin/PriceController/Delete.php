<?php

namespace App\Http\Controllers\Admin\PriceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Price;
class Delete extends Controller
{
    public function getDeletePrice($idPrice){
        $this->deletePrice($idPrice);
        return response()->json(['status'=>"success"]);
    }

    public function deletePrice($idPrice){
        return Price::deletePrice($idPrice);
    }
}
