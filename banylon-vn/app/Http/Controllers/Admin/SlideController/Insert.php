<?php

namespace App\Http\Controllers\Admin\SlideController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Slide;
class Insert extends Controller
{
    public function postAddSlideImage(Request $request){
        foreach ($request->imageDescription as $key => $value){
            Slide::addImage($request->imageDescription[$key],$request->file('imageFile.'.$key));
        }
        return redirect()->back()->with(["success"=>"Thêm Hình Ảnh Thành Công"]);
    }

    public function addImage(){
        
    }
}
