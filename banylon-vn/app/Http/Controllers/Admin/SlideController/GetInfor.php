<?php

namespace App\Http\Controllers\Admin\SlideController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Slide;
class GetInfor extends Controller
{
    public function getSlides(){
        $slides=Slide::getSlides();
        return view('admin.slide.slides',['slides'=>$slides]);
    }
}
