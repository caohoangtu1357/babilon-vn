<?php

namespace App\Http\Controllers\Admin\SlideController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Slide;
use File;
class Delete extends Controller
{
    private $path="upload/imageSlide/";

    public function getDeleteSlideImage($idImage){
        $imageDetail=$this->imageDetail($idImage);
        $this->deleteImageInFolder($imageDetail->imageLink);
        $this->deleteImageInDatabase($idImage);
        return response()->json(['status'=>"Xóa Ảnh Thành Công"]);
    }

    public function deleteImageInDatabase($idImage){
        Slide::deleteImage($idImage);
    }

    public function imageDetail($idImage){
        return Slide::imageDetail($idImage);
    }

    public function deleteImageInFolder($imageLink){
        File::delete($this->path.$imageLink);
    }
}
