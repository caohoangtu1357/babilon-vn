<?php

namespace App\Http\Controllers\Admin\ViewCountController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ViewNumber;
class GetInfor extends Controller
{
    public function getNumViewEachMonth(){
        $viewEachMonth=$this->getViewEachMonthByYear(Date('Y'));
        return response()->json(["viewEachMonth"=>$viewEachMonth]);
    }

    public function getNumViewByDayInMonth(){
        $viewNumberByMonth= ViewNumber::getNumViewByDayInMonth(Date('m'));
        $currentMonth=Date('m');
        return response()->json(['viewNumberByMonth'=>$viewNumberByMonth,'currentMonth'=>$currentMonth]);
    }
    
    public function getViewEachMonthByYear($year){
        return ViewNumber::getNumViewEachMonthByYear($year);
    }

    
    
}
