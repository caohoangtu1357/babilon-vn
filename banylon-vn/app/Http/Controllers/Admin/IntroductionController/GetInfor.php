<?php

namespace App\Http\Controllers\Admin\IntroductionController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetInfor extends Controller
{
    public function getIntroductionPage(){
        return view('admin.introduction.introduction');
    }
}
