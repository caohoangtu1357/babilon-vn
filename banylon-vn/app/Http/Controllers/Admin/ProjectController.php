<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
use App\Model\Project;
use App\Model\ProjectImage;
use DB;
use App\Model\Category;

class ProjectController extends Controller
{
    public function getProjectList(){
        

        $projectList= Project::projectList()->get();
        return view('admin.project.projectList',["projectList"=>$projectList]);
    }

    public function getAddProject(){
        $categories=Category::getCategories()->get();
        $projectTypeList=ProjectType::getProjectTypeList()->get();
        return view('admin.project.addProject',['projectTypeList'=>$projectTypeList,"categories"=>$categories]);
    }

    public function postAddProject(Request $request){
        //add pro ject detail
        $insertedId=Project::addProject($request->name,
                                        $request->customerName,
                                        $request->idProjectType,
                                        $request->address,
                                        $request->startYear,
                                        $request->idCategory
                                        );
        //add image for project
        foreach ($request->imageName as $key => $value){
            ProjectImage::insertImageProject($request->imageName[$key],$request->imageDescription[$key],$request->imageType[$key],$insertedId,$request->file('imageFile.'.$key));
        }
        return redirect()->back()->with(["success"=>"Thêm Dự Án Thành Công"]);
    }   

}
