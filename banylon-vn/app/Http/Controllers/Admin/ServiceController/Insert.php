<?php

namespace App\Http\Controllers\Admin\ServiceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Service;
class Insert extends Controller
{
    public function postAddService(Request $request){
        $this->addService($request->name,$request->idServiceType);
        return response()->json(['success'=>"success"]);
    }

    public function addService($name,$idServiceType){
        return Service::addService($name,$idServiceType);
    }

    public function getDeleteService($idService){
        return Service::deleteServiceById($idService);
    }
}
