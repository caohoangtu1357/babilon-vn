<?php

namespace App\Http\Controllers\Admin\ServiceController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Service;
class Delete extends Controller
{
    public function getDeleteService($idService){
        $this->deleteServiceById($idService);
        return response()->json(["status"=>"success"]);
    }

    public function deleteServiceById($idService){
        return Service::deleteServiceById($idService);
    }
}
