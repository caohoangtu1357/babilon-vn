<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
use DB;
class ProjectTypeController extends Controller
{
    public function getAddProjectType(){
        return view('admin.projectType.addProjectType');
    }
    public function getProjectTypeList(){
        $projectTypeList= ProjectType::projectTypeListWithTotalProject()->get();
        return view('admin.projectType.projectTypeList',['projectTypeList'=>$projectTypeList]);
    }
    public function postAddProjectType(Request $request){
        ProjectType::addProjectType($request->name);
        return response()->json(['status'=>'Đã thêm thành công']);
    }
}
