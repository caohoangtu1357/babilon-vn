<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ViewNumber;
class HomeController extends Controller
{
    public function getHomePage(){
        return view('admin.adminMainPage');
    }

    
}
