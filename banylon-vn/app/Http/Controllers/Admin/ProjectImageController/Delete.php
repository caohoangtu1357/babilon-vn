<?php

namespace App\Http\Controllers\Admin\ProjectImageController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectImage;
use File;
class Delete extends Controller
{
    private $imageProjectPath="upload/imageProject/";
    public function getDeleteImageById($idImage){
        $this->deleteImageInUploadFolder($idImage);
        $this->deleteImageInDatabase($idImage);
        return response()->json(["status"=>"success"]);
    }

    public function deleteImageInUploadFolder($idImage){
        $image= ProjectImage::getImageById($idImage)->first();
        File::delete($this->imageProjectPath.$image->imageLink);
    }

    public function deleteImageInDatabase($idImage){
        ProjectImage::deleteImageById($idImage);
    }
}
