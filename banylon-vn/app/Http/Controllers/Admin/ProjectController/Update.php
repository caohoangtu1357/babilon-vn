<?php

namespace App\Http\Controllers\Admin\ProjectController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\ProjectType;
use App\Model\Project;
use App\Model\ProjectImage;

class Update extends Controller
{
    public function getUpdateProject($idProject){
        $categories=$this->getCategories();
        $projectTypes=$this->getProjectTypes();
        $projectDetail=$this->getProjectDetail($idProject);
        $imageTypeName=$this->getArrayImageType();
        $imagesOfProject=$this->getImagesOfProject($idProject);
        return view('admin.project.projectDetail',[
                                                    'categories'=>$categories,
                                                    'projectTypes'=>$projectTypes,
                                                    'projectDetail'=>$projectDetail,
                                                    'imageTypeName'=>$imageTypeName,
                                                    'imagesOfProject'=>$imagesOfProject
                                                  ]);
    }

    public function postUpdateProject(Request $request){
        $this->updateProjectDetail($request);
        $this->insertNewImageProject($request);
        return redirect()->route('adminGetUpdateProject',['id'=>$request->id])->with(["success"=>"Cập nhật thành công"]);
    }


    public function getArrayImageType(){
        return [1=>'Ảnh Đại Diện',2=>'Ảnh Dự Án',3=>'Ảnh Thiết Kế',4=>'Ảnh Sau Thi Công'];
    }

    public function getCategories(){
        return Category::getCategories()->get();
    }

    public function getProjectTypes(){
        return ProjectType::getProjectTypeList()->get();
    }

    public function getProjectDetail($idProject){
        return Project::getProjectDetailById($idProject)->first();
    }

    public function getImagesOfProject($idProject){
        return ProjectImage::getAllImageOfProject($idProject)->get();
    }

    public function updateProjectDetail($request){
        Project::updateProjectDetail(["name"=>$request->name,
                                      "customerName"=>$request->customerName,
                                      "address"=>$request->address,
                                      "startYear"=>$request->startYear,
                                      "idProjectType"=>$request->idProjectType,
                                      "idCategory"=>$request->idCategory,
                                      "id"=>$request->id
                                     ]);
    }

    public function insertNewImageProject($request){
        foreach ($request->imageName as $key => $value){
            if($value!=""){
                ProjectImage::insertImageProject($request->imageName[$key],$request->imageDescription[$key],$request->imageType[$key],$request->id,$request->file('imageFile.'.$key));
            }
        }
    }
}
