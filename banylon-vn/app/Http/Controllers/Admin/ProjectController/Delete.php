<?php

namespace App\Http\Controllers\Admin\ProjectController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectImage;
use App\Model\Project;
use File;
class Delete extends Controller
{
    private $imageProjectPath="upload/imageProject/";
    
    
    public function getDeleteProject($idProject){
        $this->deleteImageOfProject($idProject);
        $this->deleteProjectDetail($idProject);
        return response()->json(["status"=>"Xóa Thành Công"]);
    }

    public function deleteImageOfProject($idProject){
        $images=ProjectImage::getAllImageOfProject($idProject)->get();
        $this->deleteImageInUploadFolder($images);
        $this->deleteImageInDatabase($idProject);
    }

    public function deleteProjectDetail($idProject){
        Project::deleteProjectDetail($idProject);
    }

    public function deleteImageInUploadFolder($images){
        foreach ($images as $key => $image) {
            File::delete($this->imageProjectPath.$image->imageLink);
        }
    }

    public function deleteImageInDatabase($idProject){
        ProjectImage::deleteImageByIdProject($idProject);
    }
}
