<?php

namespace App\Http\Controllers\Admin\CategoryController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;

class Delete extends Controller
{
    public function getDeleteCategory($idCategory){
        if($this->checkHasProjectBelong($idCategory)){
            return response()->json(["status"=>"false"]);
        }else{
            $this->deleteCategory($idCategory);
            return response()->json(["status"=>"success"]);
        }
    }

    public function checkHasProjectBelong($idCategory){
        if(Category::isHaveProjectBelong($idCategory)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteCategory($idCategory){
        return Category::deleteCategory($idCategory);
    }
}
