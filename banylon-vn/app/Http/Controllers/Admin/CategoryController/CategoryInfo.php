<?php

namespace App\Http\Controllers\Admin\CategoryController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
class CategoryInfo extends Controller
{
    public function getCategoryDetail($idCategory){
        $categoryDetail=$this->getCategoryById($idCategory);
        return response()->json(['categoryDetail'=>$categoryDetail]);
    }

    public function getCategoryById($idCategory){
        return Category::getCategories()->where('id',$idCategory)->first();
    }
}
