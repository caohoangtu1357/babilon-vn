<?php

namespace App\Http\Controllers\Admin\CategoryController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
class Update extends Controller
{
    public function postUpdateCategory(Request $request){
       $this->updateCategoryById($request); 
       return response()->json(["status"=>"success"]);
    }

    public function updateCategoryById($request){
        return Category::updateCategoryById($request->id,['name'=>$request->name]);
    }
}
