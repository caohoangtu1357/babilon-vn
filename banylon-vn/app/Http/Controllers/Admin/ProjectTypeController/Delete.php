<?php

namespace App\Http\Controllers\Admin\ProjectTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
class Delete extends Controller
{
    public function getDeleteProjectType($idProjectType){
        if($this->checkHasProjectBelong($idProjectType)){
            return response()->json(["status"=>"false"]);
        }else{
            $this->deleteProjectType($idProjectType);
            return response()->json(["status"=>"success"]);
        }
    }

    public function checkHasProjectBelong($idProjectType){
        if(ProjectType::isHaveProjectBelong($idProjectType)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteProjectType($idProjectType){
        return ProjectType::deleteProjectType($idProjectType);
    }
}
