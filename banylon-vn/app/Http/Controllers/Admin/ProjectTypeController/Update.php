<?php

namespace App\Http\Controllers\Admin\ProjectTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
class Update extends Controller
{
    
    public function postUpdateProjectType(Request $request){
        $this->updateProjectTypeById($request->id,$request->name);
        return response()->json(['success'=>"Đã Cập Nhật Thành Công"]);
    }

    public function updateProjectTypeById($idProjectType,$name){
        return ProjectType::updateProjectTypeById($idProjectType,$name);
    }

}
