<?php

namespace App\Http\Controllers\Admin\ProjectTypeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectType;
class ProjectTypeInfo extends Controller
{

    public function getProjectTypeDetail($idProjectType){
        $projectTypeDetail=$this->getProjectTypeDetailById($idProjectType);
        return response()->json(['projectTypeDetail'=>$projectTypeDetail]);
    }

    public function getProjectTypeDetailById($idProjectType){
        return ProjectType::getProjectTypeList()->where('id',$idProjectType)->first();
    }
}
