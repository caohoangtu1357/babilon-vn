<?php

namespace App\Http\Middleware;
use Illuminate\Session\Store;
use Closure;

class CheckExpireSessionForIncreaseViewCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $session;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pageViewed = $this->getViewedPages();

        if (!is_null($pageViewed))
        {
            $pageViewed = $this->cleanExpiredViews($pageViewed);
            $this->storePageViewed($pageViewed);
        }

        return $next($request);
    }

    private function getViewedPages()
    {
        return $this->session->get('viewedPage', null);
    }

    private function cleanExpiredViews($pageViewed)
    {
        $time = time();
        $throttleTime = 500; //after 15 minute session will clear

        return array_filter($pageViewed, function ($timestamp) use ($time, $throttleTime)
        {
            return ($timestamp + $throttleTime) > $time;
        });
    }

    private function storePageViewed($pageViewed)
    {
        $this->session->put('viewedPage', $pageViewed);
    }

}
