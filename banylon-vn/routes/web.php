<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(["middleware"=>"checkAdminLogin"],function(){
    require('admin.php');
});


Route::group(["middleware"=>"CheckExpireSessionForIncreaseViewCount"],function(){
    require('user.php');
});
require('authenticate.php');