<?php
Route::get('trang-chu-admin', [
    "as"=>"adminHomePage",
    "uses"=>"Admin\HomeController@getHomePage"
]);
Route::get('lay-luong-view-theo-ngay-trong-thang', [
    "as"=>"adminGetViewNumByMonth",
    "uses"=>"Admin\ViewCountController\GetInfor@getNumViewByDayInMonth"
]);
Route::get('lay-luong-view-tung-thang-trong-nam', [
    "as"=>"adminGetViewNumEachMonth",
    "uses"=>"Admin\ViewCountController\GetInfor@getNumViewEachMonth"
]);


/**
 * Project Route
 * 
 */
Route::get('danh-sach-du-an',[
    "as"=>"adminProjectList",
    "uses"=>"Admin\ProjectController@getProjectList"
]);

Route::get('them-du-an',[
    "as"=>"adminAddProject",
    "uses"=>"Admin\ProjectController@getAddProject"
]);

Route::post('them-du-an',[
    "as"=>"adminPostAddProject",
    "uses"=>"Admin\ProjectController@postAddProject"
]);

Route::get("xoa-du-an/{idProject}",[
    "as"=>"adminGetDeleteProject",
    "uses"=>"Admin\ProjectController\Delete@getDeleteProject"
]);

Route::get("chinh-sua-du-an/{idProject}",[
    "as"=>"adminGetUpdateProject",
    "uses"=>"Admin\ProjectController\Update@getUpdateProject"
]);

Route::post("chinh-sua-du-an",[
    "as"=>"adminPostUpdateProject",
    "uses"=>"Admin\ProjectController\Update@postUpdateProject"
]);





/**
 * Project type Route
 * 
 */

Route::get('them-loai-du-an',[
    "as"=>"adminAddProjectType",
    "uses"=>"Admin\ProjectTypeController@getAddProjectType"
]);
Route::post('them-loai-du-an',[
    "as"=>"adminPostAddProjectType",
    "uses"=>"Admin\ProjectTypeController@postAddProjectType"
]);
Route::get('danh-sach-loai-du-an',[
    "as"=>"adminProjectTypeList",
    "uses"=>"Admin\ProjectTypeController@getProjectTypeList"
]);

Route::get('xoa-loai-du-an/{idProjectType}',[
    "as"=>"adminGetDeleteProjectType",
    "uses"=>"Admin\ProjectTypeController\Delete@getDeleteProjectType"
]);


Route::post('cap-nhat-loai-du-an',[
    "as"=>"adminPostUpdateProjectType",
    "uses"=>"Admin\ProjectTypeController\Update@postUpdateProjectType"
]);

Route::get('thong-tin-loai-du-an/{idProjectType}',[
    "as"=>"adminGetProjectTypeDetail",
    "uses"=>"Admin\ProjectTypeController\ProjectTypeInfo@getProjectTypeDetail"
]);



/**
 * Image Project
 * 
 */
Route::post('them-hinh-du-an',[
    "as"=>"adminPostAddImageProject",
    "uses"=>"Admin\ProjectImageController@postAddImageProject"
]);
Route::get('chinh-sua-du-an/xoa-hinh-anh/{idImage}',[
    "as"=>"adminGetDeleteImage",
    "uses"=>"Admin\ProjectImageController\Delete@getDeleteImageById"
]);

/**
 * Project Category 
 * 
 */
Route::get('them-hang-muc-du-an',[
    "as"=>"adminGetAddCategory",
    "uses"=>"Admin\CategoryController@getAddCategory"
]);
Route::post('them-hang-muc-du-an',[
    "as"=>"adminPostAddCategory",
    "uses"=>"Admin\CategoryController@postAddCategory"
]);
Route::get('danh-sach-hang-muc',[
    "as"=>"adminGetCategoryList",
    "uses"=>"Admin\CategoryController@getCategoryList"
]);
Route::get('xoa-danh-muc-du-an/{idCategory}',[
    "as"=>"adminGetDeleteCategory",
    "uses"=>"Admin\CategoryController\Delete@getDeleteCategory"
]);
Route::get('thong-tin-hang-muc-du-an/{idCategory}',[
    "as"=>"adminGetCategoryDetail",
    "uses"=>"Admin\CategoryController\CategoryInfo@getCategoryDetail"
]);
Route::post('cap-nhat-hang-muc-du-an',[
    "as"=>"postAdminUpdateCategory",
    "uses"=>"Admin\CategoryController\Update@postUpdateCategory"
]);

/**
 * 
 * Employee Type
 */
Route::get('them-loai-nhan-vien',[
    "as"=>"getAdminAddEmployeeType",
    "uses"=>"Admin\CategoryController\Update@postUpdateCategory"
]);


/**
 * Introduction 
 * 
 */
Route::get('dieu-chinh-bai-gioi-thieu',[
    "as"=>"getAdminIntroduction",
    "uses"=>"Admin\IntroductionController\GetInfor@getIntroductionPage"
]);

/**
 * 
 * service type
 */


Route::get('them-loai-dich-vu',[
    "as"=>"getAdminInsertServiceTypePage",
    "uses"=>"Admin\ServiceTypeController\Insert@getInsertServiceTypePage"
]);
Route::post('them-loai-dich-vu',[
    "as"=>"postAdminInsertServiceTypePage",
    "uses"=>"Admin\ServiceTypeController\Insert@postInsertServiceType"
]);
Route::get('danh-sach-loai-dich-vu',[
    "as"=>"getAdminServiceTypeList",
    "uses"=>"Admin\ServiceTypeController\GetInfor@getServiceTypeList"
]);
Route::get('thong-tin-loai-dich-vu/{idServiceType}',[
    "as"=>"getAdminServiceTypeDetail",
    "uses"=>"Admin\ServiceTypeController\GetInfor@getServiceTypeDetail"
]);
Route::post('cap-nhat-loai-dich-vu',[
    "as"=>"postAdminUpdateServiceType",
    "uses"=>"Admin\ServiceTypeController\Update@postUpdateServiceType"
]);
Route::get('xoa-loai-dich-vu/{idServiceType}',[
    "as"=>"getAdminDeleteServiceType",
    "uses"=>"Admin\ServiceTypeController\Delete@getDeleteServiceType"
]);
Route::get('them-bang-gia-va-dich-vu/{idServiceType}',[
    "as"=>"getAdminInsertPriceAndServicePage",
    "uses"=>"Admin\ServiceTypeController\Insert@getInsertPriceAndServicePage"
]);

Route::post('them-bang-gia-va-dich-vu',[
    "as"=>"postAdminInsertServicesAndPrices",
    "uses"=>"Admin\ServiceTypeController\Insert@postInsertServicesAndPrices"
]);

/**
 * service 
 * 
 */
Route::post('them-bang-gia-va-dich-vu/them-dich-vu',[
    "as"=>"postAdminAddService",
    "uses"=>"Admin\ServiceController\Insert@postAddService"
]);
Route::get('them-bang-gia-va-dich-vu/xoa-dich-vu/{idService}',[
    "as"=>"getAdminDeleteService",
    "uses"=>"Admin\ServiceController\Delete@getDeleteService"
]);


/**
 * price
 * 
 */
Route::post('them-bang-gia-va-dich-vu/them-bang-gia',[
    "as"=>"postAdminAddPrice",
    "uses"=>"Admin\PriceController\Insert@postAddPrice"
]);
Route::get('them-bang-gia-va-dich-vu/xoa-bang-gia/{idPrice}',[
    "as"=>"getAdminDeletePrice",
    "uses"=>"Admin\PriceController\Delete@getDeletePrice"
]);

/**
 * Reference
 * 
 */
Route::get('them-bai-tham-khao',[
    "as"=>"getAdminAddReference",
    "uses"=>"Admin\ReferenceController\Insert@getInsert"
]);
Route::post('them-bai-tham-khao',[
    "as"=>"postAdminAddReference",
    "uses"=>"Admin\ReferenceController\Insert@postInsert"
]);
Route::get('danh-sach-bai-tham-khao',[
    "as"=>"getAdminReferences",
    "uses"=>"Admin\ReferenceController\GetInfor@getReferences"
]);

Route::get('xoa-bai-tham-khao/{id}',[
    "as"=>"getAdminDeleteReferences",
    "uses"=>"Admin\ReferenceController\Delete@getDeleteReference"
]);

Route::get('chi-tiet-bai-tham-khao/{id}',[
    "as"=>"getAdminReferenceDetail",
    "uses"=>"Admin\ReferenceController\GetInfor@getReferenceDetail"
]);


Route::post('chi-tiet-bai-tham-khao',[
    "as"=>"postAdminUpdateReference",
    "uses"=>"Admin\ReferenceController\Update@postUpdateReference"
]);

/**
 * 
 * slide
 * 
 */
Route::get('danh-sach-hinh-anh-trong-slide',[
    "as"=>"getAdminSlidesImage",
    "uses"=>"Admin\SlideController\GetInfor@getSlides"
]);


Route::post('them-hinh-anh-trong-slide',[
    "as"=>"postAdminAddSlideImage",
    "uses"=>"Admin\SlideController\Insert@postAddSlideImage"
]);

Route::get('xoa-hinh-anh-trong-slide/{id}',[
    "as"=>"getAdminDeleteSlideImage",
    "uses"=>"Admin\SlideController\Delete@getDeleteSlideImage"
]);