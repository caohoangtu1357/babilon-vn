<?php


Route::get("/",[
    "as"=>"userMainPage",
    "uses"=>"User\HomeController@getHomePage"
]);

/**
 * 
 * du an
 */
Route::get("tat-ca-du-an",[
    "as"=>"userGetAllProject",
    "uses"=>"User\ProjectController@getAllProject"
]);


Route::get('chi-tiet-du-an/{idProject}',[
    "as"=>"userGetProjectDetail",
    "uses"=>"User\ProjectController@getProjectDetail"
]);

Route::get('danh-sach-du-an-theo-loai-du-an/{id}',[
    "as"=>"userProjectsByProjectTypeId",
    "uses"=>"User\ProjectTypeController@getProjectByProjectTypeId"
]);

/**
 * introduction
 * 
 */
Route::get('gioi-thieu',[
    "as"=>"userGetIntroductionPage",
    "uses"=>"User\IntroductionController\GetInfor@getIntroducePage"
]);

/**
 * reference
 * 
 */
Route::get('tham-khao',[
    "as"=>"userGetReferencePage",
    "uses"=>"User\ReferenceController@getReferencePage"
]);














?>