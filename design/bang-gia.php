<div class="container">
		<h4 style="text-align: center ; font-weight: bold;">Hỗ trợ chu đáo, chi phí hợp lý</h4>
		<p style="text-align:center">BABYLON với thế mạnh Design & Buid, chúng tôi hiểu biết sâu sắc trong lĩnh vực của mình, sẽ
		mang đến cho bạn công trình mơ ước với chi phí hợp lý nhất, giúp bạn tiết kiệm đáng kể công
		sức và thời gian.</p>
</div>

<div class="container-fluid">
	<div class="row ">
		<div class="col-xl-3 col-md-6 col-sm-6">
			<div class="column">
				<h4>THIẾT KẾ</h4>
				<h5>200,000 Vnđ/m2</h5>
				<p>Nội thất, kiến trúc nhà phố</p>
			
				<h5>300,000 Vnđ/m2</h5>
				<p>Biệt thự phong cách cổ điển</p>
				<p>Căn hộ</p>
				</div>
			<div class="column1">
				<p><span>&#10003;</span> Bản vẽ kiến trúc</p>
				<p><span>&#10003;</span> Bản vẽ kết cấu</p>
				<p><span>&#10003;</span> Bản vẽ M&E</p>
				<p><span>&#10003;</span> Phối cảnh 3D mặt tiền</p>
				<p><span>&#10003;</span> Phối cảnh 3D nội thất</p>
				<p><span>&#10003;</span> Giám sát quyền tác giả</p>
			
			</div>
		</div>
		<div class="col-xl-3 col-md-6 col-sm-6">
			<div class="column">
				<h4>THI CÔNG PHẦN THÔ</h4>
				<h5>3,300,000 Vnđ/m2</h5>
				<p>Nhà phố</p>
			
				<h5>3,500,000 Vnđ/m2</h5>
				<p class="abc">Biệt thự</p>
			</div>
			<div class="column1">
				<p><span>&#10003;</span> Nhân công, vật tư phần thô</p>
				<p><span>&#10003;</span> Nhân công, vật tư phần điện nước đi âm</p>
				<p><span>&#10003;</span> Nhân công phần hoàn thiện</p>
			
			</div>
		</div>
		<div class="col-xl-3 col-md-6 col-sm-6">
			<div class="column">
				<h4>THI CÔNG HOÀN THIỆN</h4>
				<h5>2,500,000 Vnđ/m2</h5>
				<p>Hoàn thiện ở mức độ khá</p>
			
				<h5>3,00,000 Vnđ/m2</h5>
				<p>Hoàn thiện ở mức độ tốt</p>
			</div>
			<div class="column1">
				<p><span>&#10003;</span> Trần thạch cao</p>
				<p><span>&#10003;</span> Vật tư sơn nước</p>
				<p><span>&#10003;</span> Gạch ốp lát</p>
				<p><span>&#10003;</span> Cửa đi và cửa sổ</p>
				<p><span>&#10003;</span> Lan can, tay vịn cầu thang, ban công</p>
				<p><span>&#10003;</span> Đá trang trí</p>
				<p><span>&#10003;</span> Thiết bị điện</p>
				<p><span>&#10003;</span> Thiết bị vệ sinh</p>
							
			</div>
		</div>
		<div class="col-xl-3 col-md-6 col-sm-6 color">
			<div class="column2">
				<h4>GIẢM PHÍ THIẾT KẾ</h4>
				<p><span>&#10003;</span>Khi kí hợp đồng thi công</p>
				<p><span>&#10003;</span>Đảm bảo sản phẩm thực tế
				đạt trên 80% thiết kế.</p>
			
			</div>
			<div class="column1">
		
			</div>
		</div>
	</div>
</div>
<br>