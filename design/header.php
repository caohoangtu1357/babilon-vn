<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Babylon - Kiên trúc, xây dựng và nội thất</title>
	<!-- Import Boostrap css, js, font awesome here -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">       
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link href="http://babylonvietnam.vn/style1.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<!--header-->
<div class="wrapper">
	<!-- Carousel -->
	<div id="slides" class="carousel slide" data-ride="carousel">
		<ul class="carousel-indicators">
			<li data-target="#slides" data-slide-to="0" class="active"></li>
			<li data-target="#slides" data-slide-to="1"></li>
			<li data-target="#slides" data-slide-to="2"></li>		
			<li data-target="#slides" data-slide-to="3"></li>
				<li data-target="#slides" data-slide-to="4"></li>
		</ul>
		<div class="carousel-inner">
			<div class="carousel-caption">
					<h1 >KIẾN TRÚC - XÂY DỰNG - NỘI THẤT</h1>
					
				</div>
				<div class="carousel-item active">
			    	<img src="http://babylonvietnam.vn/anhdau1.jpg">
		       	</div>
			<div class="carousel-item ">
				<img src="http://babylonvietnam.vn/anhdau2.jpg">
				
			</div>
		
			<div class="carousel-item">
				<img src="http://babylonvietnam.vn/anhdau3.jpg">
			</div>
			<div class="carousel-item">
				<img src="http://babylonvietnam.vn/anhdau4.jpg">
			</div>
				<div class="carousel-item">
				<img src="http://babylonvietnam.vn/anhdau5.jpg">
			</div>
		</div>
	</div>
<!-- end carousel-->
	<!-- Navigation -->
	<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-branch" href="../">
				<img src="http://babylonvietnam.vn/Babylon%20logo.png" >
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" 
				data-target="#navbarResponsive">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto menu-mb">
				    <li class="nav-item">
						<a class="nav-link" href="#">Hotline: 0932 131 694</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="http://babylonvietnam.vn">Giới thiệu</a>
					</li>
					 <li class="nav-item dropdown">
        				<a href="http://babylonvietnam.vn/Du-an" class=" nav-link">Dự án</a>
        				<span  id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        				
       					 </span>
       					 <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				          <a class="dropdown-item" href="#">Ảnh thực tế sau thi công</a>
				          <a class="dropdown-item" href="#">Căn hộ, penthouse</a>
				          <a class="dropdown-item" href="#">Biệt thự</a>
				          <a class="dropdown-item" href="#">Nhà phố</a>
				          <a class="dropdown-item" href="#">Khách sạn, resort</a>
				          <a class="dropdown-item" href="#">Showroom,shop</a>
				           <a class="dropdown-item" href="#">Building,office</a>
				          <a class="dropdown-item" href="#">Concept</a>
				        </div>
    				</li>
					<li class="nav-item">
						<a class="nav-link" href="http://babylonvietnam.vn/Bang-gia">Bảng giá</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Tham khảo</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Liên hệ</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>

<!--end header-->