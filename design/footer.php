<div class="container-fluid">
<div class="footer">
	<div class="row">
		<div class="col-sm-7">
			<h5> CAM KẾT</h5>
			<p ><i class="fas fa-circle"></i> Với lợi
thế về Design & Build chúng tôi sẽ mang đến một sản phẩm phù hợp nhất với nhu cầu của bạn.</p>
			<p ><i class="fas fa-circle"></i> Sẵn sàng hỗ trợ mọi lúc và đưa ra lời khuyên phù hợp nhất.
</p>
			<p><i class="fas fa-circle"></i> Chúng tôi sẽ mang đến cho bạn sản phẩm tốt nhất với chi phi hợp lý.</p>
			<p><i class="fas fa-circle"></i> Cung cấp đầy đủ các dịch vụ cho đến khi bạn dọn nhà vào ở.</p> 
		</div>
		<div class="col-sm-5">
			<h5>TRỤ SỞ CHÍNH</h5>
			<p ><i class="fas fa-circle"></i> 27/2A3 Huỳnh Tịnh Của, P8, Q3, Tp.HCM</p>
			<p><i class="fas fa-circle"></i> Email : support@babylonvietnam.vn</p>
			<p><i class="fas fa-circle"></i> Hotline : 0932 131 694</p>
			<p class="cangiua"><img src="http://babylonvietnam.vn/Babylon%20logo.png"></p>
		</div>
	</div>
</div>
</div>
<div class="hotline"><i class="fa fa-phone"></i> Hotline : <a>0932 131 694</a> (Hỗ trợ miễn phí)</div>
<script>
	
$('.navbar-nav .nav-link').click(function(){
    $('.navbar-nav .nav-link').removeClass('active');
    $(this).addClass('active');
})

</script>
</body>
</html>	
